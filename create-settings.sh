#!/bin/bash

# Garante que temos o diretorio de trabalho para o build
mkdir -p /tmp

# Cria um arquivo de settings temporario para uso do maven
echo Criando settings para acesso ao Nexus...
echo CI_USERNAME=$CI_USERNAME
echo "SHA1(CI_PASSWORD)= $(echo $CI_PASSWORD | sha1sum)"

cat > /tmp/build-settings.xml <<EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<settings 
	xmlns="http://maven.apache.org/SETTINGS/1.1.0" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
	
	<servers>
		<server>
		  <id>nexus</id>
		  <username>$CI_USERNAME</username>
		  <password>$CI_PASSWORD</password>
		</server>
		<server>
		  <id>lighthouse</id>
		  <username>$CI_USERNAME</username>
		  <password>$CI_PASSWORD</password>
		</server>
		<server>
		  <id>lighthouse-snapshots</id>
		  <username>$CI_USERNAME</username>
		  <password>$CI_PASSWORD</password>
		</server>
		<server>
		  <id>lighthouse-releases</id>
		  <username>$CI_USERNAME</username>
		  <password>$CI_PASSWORD</password>
		</server>
		
		
	</servers>
  
    <mirrors>
		<mirror>
		  <id>nexus</id>
		  <mirrorOf>*,!andromda-legacy</mirrorOf>
		  <name>Local Nexus Proxy</name>
		  <url>$LIGHTHOUSE_REPO</url>
		</mirror>			
	</mirrors>
	
	<profiles>
		<profile>
		  <id>lighthouse</id>
		  <properties>
			<altReleaseDeploymentRepository>lighthouse-releases::default::https://ssh.lighthouse.com.br/nexus/repository/lighthouse-releases/</altReleaseDeploymentRepository>		
			<altSnapshotDeploymentRepository>lighthouse-snapshots::default::https://ssh.lighthouse.com.br/nexus/repository/lighthouse-snapshots/</altSnapshotDeploymentRepository>			
		  </properties>
		</profile>	
	</profiles>
	
   <activeProfiles>
    <activeProfile>lighthouse</activeProfile>
  </activeProfiles>	
  
</settings>



EOF