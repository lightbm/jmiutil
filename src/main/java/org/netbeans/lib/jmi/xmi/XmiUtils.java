// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import java.util.Locale;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import java.util.HashMap;

public abstract class XmiUtils
{
    private static final String TEXT_NODE_NAME = "#text";
    private static final String COMMENT_NODE_NAME = "#comment";
    private static HashMap namespaces;
    
    public static String getXmiAttrValueAsString(final Node node, final String attributeName) {
        String result = null;
        if (node != null) {
            result = getAttributeValueAsString(node, getShortName(attributeName));
            if (result == null) {
                result = getElementValueAsString(node, attributeName);
            }
        }
        return result;
    }
    
    public static List getXmiRefValue(final Node node, final String attributeName) {
        final List value = new ArrayList();
        if (node != null) {
            String attr = getAttributeValueAsString(node, getShortName(attributeName));
            if (attr == null) {
                final Node refNode = getChildNode(node, attributeName);
                if (refNode != null) {
                    final NodeList values = refNode.getChildNodes();
                    for (int i = 0; i < values.getLength(); ++i) {
                        if (!isTextNode(values.item(i))) {
                            value.add(getAttributeValueAsString(values.item(i), "xmi.idref"));
                        }
                    }
                }
            }
            else {
                int pos;
                while ((pos = attr.indexOf(32)) > -1) {
                    if (pos > 0) {
                        value.add(attr.substring(0, pos));
                    }
                    attr = attr.substring(pos + 1);
                }
                value.add(attr);
            }
        }
        return value;
    }
    
    public static String getAttributeValueAsString(final Node node, final String attributeName) {
        String result = null;
        if (node != null && node.hasAttributes()) {
            final Node attribute = node.getAttributes().getNamedItem(attributeName);
            if (attribute != null) {
                result = attribute.getNodeValue();
            }
        }
        return result;
    }
    
    public static String getElementValueAsString(final Node node, final String attributeName) {
        String result = null;
        if (node != null) {
            Node attributeNode = getChildNode(node, attributeName);
            if (attributeNode != null) {
                attributeNode = attributeNode.getFirstChild();
                if (attributeNode == null) {
                    result = "";
                }
                else {
                    result = attributeNode.getNodeValue();
                }
            }
        }
        return result;
    }
    
    public static Node getChildNode(final Node parentNode, final String childNodeName) {
        Node result = null;
        if (parentNode != null && childNodeName != null) {
            final NodeList children = parentNode.getChildNodes();
            for (int cnt = 0; cnt < children.getLength(); ++cnt) {
                final Node child = children.item(cnt);
                if (childNodeName.equals(resolveFullName(child))) {
                    result = child;
                    break;
                }
            }
        }
        return result;
    }
    
    public static String resolveFullName(final Node element) {
        if (isXmiNode(element)) {
            return element.getNodeName();
        }
        return resolveFullNameAsString(element.getNodeName());
    }
    
    public static String resolveFullNameAsString(final String fullName) {
        if (fullName == null) {
            return null;
        }
        final int index;
        if ((index = fullName.indexOf(58)) > 0) {
            return XmiUtils.namespaces.get(fullName.substring(0, index)) + "." + fullName.substring(index + 1);
        }
        return fullName;
    }
    
    public static String getShortName(final String fullyQualifiedName) {
        String result = fullyQualifiedName;
        if (fullyQualifiedName != null && fullyQualifiedName.toUpperCase(Locale.US).indexOf("XMI.") > -1) {
            return fullyQualifiedName;
        }
        if (result != null && result.indexOf(":") > -1) {
            result = result.substring(result.indexOf(":") + 1);
        }
        if (result != null && result.indexOf(".") > -1) {
            result = result.substring(result.lastIndexOf(".") + 1);
        }
        return result;
    }
    
    public static boolean isTextNode(final Node node) {
        boolean result = false;
        if (node != null) {
            result = (node.getNodeName().equals("#text") || node.getNodeName().equals("#comment"));
        }
        return result;
    }
    
    public static boolean isXmiNavigationNode(final Node node) {
        boolean result = false;
        if (node != null) {
            result = (node.getNodeName().equals("xmi.id") || result);
            result = (node.getNodeName().equals("xmi.uuid") || result);
            result = (node.getNodeName().equals("xmi.label") || result);
            result = (node.getNodeName().equals("xmi.idref") || result);
        }
        return result;
    }
    
    public static boolean isXmiNode(final Node node) {
        boolean result = false;
        if (node != null) {
            result |= isXmiNavigationNode(node);
            result |= node.getNodeName().equals("XMI.any");
        }
        return result;
    }
    
    static {
        (XmiUtils.namespaces = new HashMap()).put("Model", "Model");
        XmiUtils.namespaces.put("UML", "Model_Management");
        XmiUtils.namespaces.put("CWMOLAP", "Olap");
        XmiUtils.namespaces.put("CWMTFM", "Transformation");
        XmiUtils.namespaces.put("CWM", "BusinessInformation");
        XmiUtils.namespaces.put("CWMRDB", "Relational");
    }
    
    public static class XmiNodeIterator
    {
        private Node node;
        private String attrName;
        private String attrValue;
        private String nodeName;
        private NodeList childNodes;
        private Node currentNode;
        private int index;
        
        public XmiNodeIterator(final Node node, final String nodeName) {
            this.node = null;
            this.childNodes = null;
            this.currentNode = null;
            this.index = 0;
            this.attrName = null;
            this.attrValue = null;
            this.nodeName = nodeName;
            this.node = node;
            this.childNodes = node.getChildNodes();
            this.findNext();
        }
        
        public XmiNodeIterator(final Node node, final String attrName, final String attrValue) {
            this.node = null;
            this.childNodes = null;
            this.currentNode = null;
            this.index = 0;
            this.attrName = attrName;
            this.attrValue = attrValue;
            this.nodeName = null;
            this.node = node;
            this.childNodes = node.getChildNodes();
            this.findNext();
        }
        
        public XmiNodeIterator(final Node node) {
            this(node, null, null);
        }
        
        public boolean hasNext() {
            return this.currentNode != null;
        }
        
        public Node next() {
            final Node result = this.currentNode;
            this.findNext();
            return result;
        }
        
        private void findNext() {
            for (int i = this.index; i < this.childNodes.getLength(); ++i) {
                final Node sn = this.childNodes.item(i);
                if (!XmiUtils.isTextNode(sn)) {
                    if (this.attrName != null) {
                        if (XmiUtils.getXmiAttrValueAsString(sn, this.attrName) == null) {
                            continue;
                        }
                        if (!XmiUtils.getXmiAttrValueAsString(sn, this.attrName).equals(this.attrValue)) {
                            continue;
                        }
                    }
                    if (this.nodeName == null || XmiUtils.resolveFullName(sn).substring(this.nodeName.lastIndexOf(46) + 1).equals(this.nodeName.substring(this.nodeName.lastIndexOf(46) + 1))) {
                        this.currentNode = sn;
                        this.index = i + 1;
                        return;
                    }
                }
            }
            this.currentNode = null;
            this.index = this.childNodes.getLength();
        }
    }
}
