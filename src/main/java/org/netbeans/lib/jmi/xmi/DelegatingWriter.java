// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import javax.jmi.reflect.RefPackage;
import java.io.IOException;
import java.util.Collection;
import java.io.OutputStream;
import org.netbeans.api.xmi.XMIOutputConfig;
import org.netbeans.api.xmi.XMIWriter;

public class DelegatingWriter extends XMIWriter
{
    public static final String XMI_VERSION_20 = "2.0";
    private XMIOutputConfig config;
    
    public DelegatingWriter() {
        this.config = new OutputConfig();
    }
    
    public DelegatingWriter(final XMIOutputConfig config) {
        this.config = config;
    }
    
    public XMIOutputConfig getConfiguration() {
        return this.config;
    }
    
    public void write(final OutputStream stream, final String uri, final Collection objects, final String xmiVersion) throws IOException {
        this.getWriter(xmiVersion).write(stream, uri, objects, xmiVersion);
    }
    
    public void write(final OutputStream stream, final String uri, final RefPackage extent, final String xmiVersion) throws IOException {
        this.getWriter(xmiVersion).write(stream, uri, extent, xmiVersion);
    }
    
    private WriterBase getWriter(final String xmiVersion) {
        if (xmiVersion != null && xmiVersion.equals("2.0")) {
            return new XMI20Writer(this.config);
        }
        return new WriterBase(this.config);
    }
}
