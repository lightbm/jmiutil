/*
 *                 Sun Public License Notice
 * 
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 * 
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2001 Sun
 * Microsystems, Inc. All Rights Reserved.
 */

package org.netbeans.lib.jmi.xmi;

import java.util.*;

import org.netbeans.lib.jmi.util.*;

import javax.jmi.reflect.*;
import javax.jmi.model.*;

public class ElementsCache extends Object {
     
    // variables ................................................................
    
    // cache storing structures' fields
    private HashMap structureFields_cache = new HashMap ();
    
    // caches storing instance-scoped and class-scoped attributes and references
    private HashMap instanceAttributes_cache = new HashMap ();
    private HashMap classAttributes_cache = new HashMap ();
    private HashMap references_cache = new HashMap ();    
    private HashMap subtypes_cache = null;
    
    private RefPackage extent;
    
    // storage of all outermost packages
    private HashMap outermostPackages = null;
    // global cache used by @link #findOutermostPackages method
    private HashMap trackedPackages = null;
    // cache for resolved proxies enabling Enum and Struct creation and Association proxies
    private HashMap proxies_cache = new HashMap ();
    
    // init .....................................................................
    
    public ElementsCache (RefPackage extent) {
        this.extent = extent;        
    }
    
    // methods ..................................................................
    
    /**
     * Detects all outermost packages related to the input extents and stores them.
     * Stores all related namespaces as well.
     */
    private void findOutermostPackages (RefPackage pkg) {
        if (trackedPackages.get (pkg) != null)
            return;
        String name;
        MofPackage metaObj = (MofPackage) pkg.refMetaObject ();
        if (metaObj.getContainer() == null) {             
            Iterator iter = metaObj.getQualifiedName ().iterator ();
            String fqName = (String) iter.next ();
            while (iter.hasNext ())
                fqName = fqName.concat (".").concat ((String) iter.next ());
            outermostPackages.put (fqName, pkg);
        }        
        trackedPackages.put (pkg, pkg);
        Iterator iter =  pkg.refAllPackages ().iterator ();
        while (iter.hasNext ()) {
            findOutermostPackages ((RefPackage) iter.next ());
        }
    }
    
    /**
     * Finds all attributes and references belonging to a mof class and caches them.
     */
    private void cacheContainedElements (MofClass mofClass) {
        List temp = new LinkedList ();
        List superClasses = mofClass.allSupertypes ();
        Namespace namespace = null;
        Iterator it = superClasses.iterator ();
        while (it.hasNext ()) {
            namespace = (Namespace) it.next ();
            temp.addAll (namespace.getContents ());
        }
        temp.addAll (mofClass.getContents ());
        List instanceAttributes = new LinkedList ();
        List classAttributes = new LinkedList ();
        List references = new LinkedList ();
        
        it = temp.iterator ();
        while (it.hasNext ()) {
            RefObject refObject = (RefObject) it.next ();
            if (refObject instanceof Feature) {
                boolean instanceLevel = ((Feature) refObject).getScope ().equals (ScopeKindEnum.INSTANCE_LEVEL);
                if ((refObject instanceof Attribute) && (!((Attribute) refObject).isDerived ())) {
                    if (instanceLevel) {
                        instanceAttributes.add (refObject);
                    } else {
                        classAttributes.add (refObject);
                    }
                } else if (refObject instanceof Reference) {
                    Association assoc = (Association) ((Reference) refObject).
                        getReferencedEnd ().getContainer ();
                    if (!assoc.isDerived ())
                        references.add (refObject);
                } // else
            } // if (refObject instanceof Feature)
        } // while
        instanceAttributes_cache.put (mofClass, instanceAttributes);
        classAttributes_cache.put (mofClass, classAttributes);
        references_cache.put (mofClass, references);                
    }

    /**
     * For a given mof class, returns list of all instance-scoped attributes 
     * (references are not included).
     *
     * @param mofClass
     * @return list of all non-derived instance-scoped attributes (including inherited ones)
     */
    public List instanceAttributes (MofClass mofClass) {
        List list = (List) instanceAttributes_cache.get (mofClass);
        if (list == null) {
            cacheContainedElements (mofClass);
            list = (List) instanceAttributes_cache.get (mofClass);
        }
        return list;
    }
    
    /**
     * Returns attributes defined in the class (inhretited attributes are not included).
     */
    public List localInstanceAttributes (MofClass mofClass) {
        Iterator it = mofClass.getContents ().iterator ();
        List result = new LinkedList ();
        while (it.hasNext ()) {
            Object refObject = it.next ();
            if (refObject instanceof Feature) {
                boolean instanceLevel = ((Feature) refObject).getScope ().equals (ScopeKindEnum.INSTANCE_LEVEL);
                if (instanceLevel && (refObject instanceof Attribute) && (!((Attribute) refObject).isDerived ())) {                    
                    result.add (refObject);                    
                } // if
            } // if
        } // while
        return result;
    }
    
    /**
     * For a given mof class, returns list of all class-scoped attributes.    
     *
     * @param mofClass
     * @return list of all non-derived class-scoped attributes (including inherited ones)
     */
    public List classAttributes (MofClass mofClass) {
        List list = (List) classAttributes_cache.get (mofClass);
        if (list == null) {
            cacheContainedElements (mofClass);
            list = (List) classAttributes_cache.get (mofClass);
        }
        return list;
    }
    
    /**
     * For a given mof class, returns list of all references.
     *
     * @param mofClass
     * @return list of all non-derived references (including inherited ones)
     */
    public List references (MofClass mofClass) {
        List list = (List) references_cache.get (mofClass);
        if (list == null) {
            cacheContainedElements (mofClass);
            list = (List) references_cache.get (mofClass);
        }
        return list;
    }
    
    /**
     * Returns references defined in the class (inhretited references are not included).
     */
    public List localReferences (MofClass mofClass) {
        Iterator it = mofClass.getContents ().iterator ();
        List result = new LinkedList ();
        while (it.hasNext ()) {
            Object refObject = it.next ();
            if (refObject instanceof Feature) {
                boolean instanceLevel = ((Feature) refObject).getScope ().equals (ScopeKindEnum.INSTANCE_LEVEL);
                if (instanceLevel && (refObject instanceof Reference)) {
                    Association assoc = (Association) ((Reference) refObject).
                        getReferencedEnd ().getContainer ();
                    if (!assoc.isDerived ())
                        result.add (refObject);                    
                } // if
            } // if
        } // while
        return result;
    }
    
    /**
     * Returns list of all fields belonging to the given StructureType.
     */
    public List structureFields (StructureType type) {
        List fields = (List) structureFields_cache.get (type);
        if (fields != null)
            return fields;
        // find fields and cache them
        fields = new LinkedList ();
        Iterator content = type.getContents ().iterator ();
        while (content.hasNext ()) {
            Object element = content.next ();
            if (element instanceof StructureField)
                fields.add (element);
        } // while
        structureFields_cache.put (type, fields);
        return fields;
    }
    
    /**
     * Finds proxy object related to the container of a given meta model element.
     *
     * @param element meta model element
     * @return related proxy object or null if proxy cannot be found
     */
    public RefBaseObject findProxy (ModelElement element) {
        if (outermostPackages == null) {
            trackedPackages = new HashMap ();
            outermostPackages = new HashMap ();
            findOutermostPackages (extent);
        }
        RefBaseObject proxy = (RefBaseObject) proxies_cache.get (element);
        if (proxy != null)
            return proxy;
        LinkedList path = new LinkedList ();
        ModelElement container = element.getContainer ();
        while (container != null) {
            path.add (container);
            container = container.getContainer ();
        }
        MofPackage mofPackage = (MofPackage) path.removeLast ();
        RefPackage refPackage = (RefPackage) outermostPackages.get (mofPackage.getName ());
        if (refPackage == null) {            
            Iterator iter = outermostPackages.entrySet ().iterator ();
            while (iter.hasNext ()) {
                RefPackage ref = (RefPackage) ((Map.Entry) iter.next ()).getValue ();
                MofPackage meta = (MofPackage) ref.refMetaObject ();
                if (meta.allSupertypes().contains(mofPackage)) {
                    refPackage = ref;
                    break;
                }
            } // while
        } // if

        if (refPackage == null)
            return null;        
        if (path.size () == 0)
            proxy = refPackage;        
        while (path.size () > 0) {
            ModelElement elem = (ModelElement) path.removeLast ();
            if (elem instanceof MofPackage) {
                refPackage = refPackage.refPackage (elem);
                if (path.size () == 0)
                    proxy = refPackage;
            } else {
                if ((elem instanceof MofClass) && (path.size () == 0)) {
                    RefClass refClass = refPackage.refClass (elem);                    
                    proxy = refClass;
                } else
                    break;
            } // else
        } // while
        if (proxy != null)
            proxies_cache.put (element, proxy);
        return proxy;
    }
    
    /**
     * @return All subclassses of the given class (including non-direct subclasses and the class itself).
     */
    public Set getAllSubtypes (MofClass clazz) {
        if (subtypes_cache == null) {
            subtypes_cache = new HashMap ();
            cacheSubtypes (extent);
        }
        return (Set) subtypes_cache.get (clazz);        
    }
    
    private void cacheSubtypes (RefPackage pkg) {
        Iterator iter = pkg.refAllClasses ().iterator ();
        while (iter.hasNext ()) {
            MofClass mofClass = (MofClass) ((RefClass) iter.next ()).refMetaObject ();            
            Iterator supertypes = mofClass.allSupertypes ().iterator ();
            while (supertypes.hasNext ()) {
                MofClass superClass = (MofClass) supertypes.next ();
                Set list = (Set) subtypes_cache.get (superClass);
                if (list == null) {
                    list = new HashSet ();
                    subtypes_cache.put (superClass, list);
                } // if
                list.add (mofClass);
            } // while
            // add this mofClass as its own subtype
            Set list = (Set) subtypes_cache.get (mofClass);
            if (list == null) {
                list = new HashSet ();
                subtypes_cache.put (mofClass, list);
            } // if
            list.add (mofClass);
        } // while
        iter = pkg.refAllPackages ().iterator ();
        while (iter.hasNext ())
            cacheSubtypes ((RefPackage) iter.next ());
    }        
    
}
