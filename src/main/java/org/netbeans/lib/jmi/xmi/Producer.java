// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.xml.sax.Locator;
import org.xml.sax.helpers.LocatorImpl;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.ErrorHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import javax.jmi.reflect.RefPackage;
import java.util.Collection;
import org.netbeans.api.xmi.XMIOutputConfig;
import org.netbeans.api.xmi.sax.XMIProducer;

public class Producer extends XMIProducer
{
    private XMIOutputConfig config;
    private Collection objects;
    private RefPackage extent;
    private String xmiVersion;
    private DTDHandler dtdHandler;
    private ContentHandler contentHandler;
    private EntityResolver entityResolver;
    private ErrorHandler errorHandler;
    private ContentHandler defaultWriter;
    
    public Producer() {
        this.objects = null;
        this.extent = null;
        this.xmiVersion = null;
        this.dtdHandler = null;
        this.contentHandler = null;
        this.entityResolver = null;
        this.errorHandler = null;
        this.defaultWriter = new DefaultWriter();
        this.config = new OutputConfig();
    }
    
    public Producer(final XMIOutputConfig config) {
        this.objects = null;
        this.extent = null;
        this.xmiVersion = null;
        this.dtdHandler = null;
        this.contentHandler = null;
        this.entityResolver = null;
        this.errorHandler = null;
        this.defaultWriter = new DefaultWriter();
        this.config = config;
    }
    
    public void setSource(final Collection objects) {
        this.objects = objects;
        this.extent = null;
    }
    
    public void setSource(final RefPackage extent) {
        this.extent = extent;
        this.objects = null;
    }
    
    public Object getSource() {
        if (this.objects == null) {
            return this.extent;
        }
        return this.objects;
    }
    
    public void setXmiVersion(final String xmiVersion) {
        this.xmiVersion = xmiVersion;
    }
    
    public String getXmiVersion() {
        return this.xmiVersion;
    }
    
    public XMIOutputConfig getConfiguration() {
        return this.config;
    }
    
    public ContentHandler getContentHandler() {
        return this.contentHandler;
    }
    
    public DTDHandler getDTDHandler() {
        return this.dtdHandler;
    }
    
    public EntityResolver getEntityResolver() {
        return this.entityResolver;
    }
    
    public ErrorHandler getErrorHandler() {
        return this.errorHandler;
    }
    
    public boolean getFeature(final String name) throws SAXNotSupportedException {
        throw new SAXNotSupportedException((String)null);
    }
    
    public Object getProperty(final String name) throws SAXNotSupportedException {
        throw new SAXNotSupportedException((String)null);
    }
    
    public void parse(final InputSource input) throws IOException, SAXException {
        this.parse(input.getSystemId());
    }
    
    public void parse(final String systemId) throws IOException, SAXException {
        final ContentHandler handler = (this.contentHandler != null) ? this.contentHandler : this.defaultWriter;
        final LocatorImpl locator = new LocatorImpl();
        locator.setSystemId(systemId);
        handler.setDocumentLocator(locator);
        if (this.extent != null) {
            this.getWriter().write(this.contentHandler, systemId, this.extent, this.xmiVersion);
        }
        else {
            if (this.objects == null) {
                throw new SAXException("Source not specified.");
            }
            this.getWriter().write(this.contentHandler, systemId, this.objects, this.xmiVersion);
        }
    }
    
    public void setContentHandler(final ContentHandler handler) {
        this.contentHandler = handler;
    }
    
    public void setDTDHandler(final DTDHandler handler) {
        this.dtdHandler = handler;
    }
    
    public void setEntityResolver(final EntityResolver resolver) {
        this.entityResolver = resolver;
    }
    
    public void setErrorHandler(final ErrorHandler handler) {
        this.errorHandler = handler;
    }
    
    public void setFeature(final String name, final boolean value) throws SAXNotSupportedException {
        throw new SAXNotSupportedException((String)null);
    }
    
    public void setProperty(final String name, final Object value) throws SAXNotSupportedException {
        throw new SAXNotSupportedException((String)null);
    }
    
    private WriterBase getWriter() {
        if (this.xmiVersion != null && this.xmiVersion.equals("2.0")) {
            return new XMI20Writer(this.config);
        }
        return new WriterBase(this.config);
    }
}
