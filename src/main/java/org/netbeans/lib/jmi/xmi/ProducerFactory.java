// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIOutputConfig;
import org.netbeans.api.xmi.sax.XMIProducer;
import org.netbeans.api.xmi.sax.XMIProducerFactory;

public class ProducerFactory extends XMIProducerFactory
{
    public XMIProducer createXMIProducer() {
        return new Producer();
    }
    
    public XMIProducer createXMIProducer(final XMIOutputConfig configuration) {
        return new Producer(configuration);
    }
}
