// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.xml.sax.Locator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.netbeans.api.xmi.XMIInputConfig;
import javax.jmi.reflect.RefPackage;
import org.netbeans.api.xmi.sax.XMIConsumer;

public class Consumer extends XMIConsumer
{
    private RefPackage extent;
    private XMIInputConfig config;
    private XmiSAXReader reader;
    
    public Consumer() {
        this(null);
    }
    
    public Consumer(final XMIInputConfig cfg) {
        this.extent = null;
        this.config = new InputConfig();
        if (cfg != null) {
            this.config.setReferenceResolver(cfg.getReferenceResolver());
        }
    }
    
    public void setExtent(final RefPackage extent) {
        this.extent = extent;
    }
    
    public RefPackage getExtent() {
        return this.extent;
    }
    
    public XMIInputConfig getConfiguration() {
        return this.config;
    }
    
    public void startDocument() throws SAXException {
        if (this.reader != null) {
            throw new SAXException("Consuming is in process, cannot start a new document.");
        }
        if (this.extent == null) {
            throw new SAXException("No target extent is set.");
        }
        (this.reader = new XmiSAXReader(this.config)).initConsumer(this.extent);
    }
    
    public void endDocument() throws SAXException {
        this.check();
        this.reader.endDocument();
        this.reader = null;
    }
    
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        this.check();
        this.reader.characters(ch, start, length);
    }
    
    public void startElement(final String namespaceURI, final String localName, final String qName, final Attributes atts) throws SAXException {
        this.check();
        this.reader.startElement(namespaceURI, localName, qName, atts);
    }
    
    public void endElement(final String namespaceURI, final String localName, final String qName) throws SAXException {
        this.check();
        this.reader.endElement(namespaceURI, localName, qName);
    }
    
    public void endPrefixMapping(final String prefix) throws SAXException {
    }
    
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
    }
    
    public void processingInstruction(final String target, final String data) throws SAXException {
    }
    
    public void setDocumentLocator(final Locator locator) {
    }
    
    public void skippedEntity(final String name) throws SAXException {
    }
    
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
    }
    
    private void check() throws SAXException {
        if (this.reader == null) {
            throw new SAXException("Consumer not started.");
        }
    }
}
