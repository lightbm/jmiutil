// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import java.util.Collection;
import java.io.IOException;
import javax.jmi.reflect.RefPackage;
import java.io.OutputStream;
import javax.jmi.xmi.XmiWriter;

public class XMIWriterImpl implements XmiWriter
{
    private DelegatingWriter writer;
    
    public XMIWriterImpl() {
        this.writer = new DelegatingWriter();
    }
    
    public void write(final OutputStream stream, final RefPackage extent, final String xmiVersion) throws IOException {
        this.writer.write(stream, extent, xmiVersion);
    }
    
    public void write(final OutputStream stream, final Collection objects, final String xmiVersion) throws IOException {
        this.writer.write(stream, objects, xmiVersion);
    }
}
