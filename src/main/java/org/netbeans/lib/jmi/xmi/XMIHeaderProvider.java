// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import java.io.Writer;

public interface XMIHeaderProvider
{
    void writeHeader(final Writer p0);
}
