// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIReferenceResolver;
import org.netbeans.api.xmi.XMIInputConfig;

public class InputConfig extends XMIInputConfig
{
    private XMIReferenceResolver resolver;
    private XMIHeaderConsumer headerConsumer;
    private UnknownElementsListener unknownElemsListener;
    private boolean ignoreUnknownElements;
    
    public InputConfig() {
        this.resolver = null;
        this.headerConsumer = null;
        this.unknownElemsListener = null;
        this.ignoreUnknownElements = false;
    }
    
    public void setReferenceResolver(final XMIReferenceResolver resolver) {
        this.resolver = resolver;
    }
    
    public XMIReferenceResolver getReferenceResolver() {
        return this.resolver;
    }
    
    public void setHeaderConsumer(final XMIHeaderConsumer consumer) {
        this.headerConsumer = consumer;
    }
    
    public XMIHeaderConsumer getHeaderConsumer() {
        return this.headerConsumer;
    }
    
    public boolean isUnknownElementsIgnored() {
        return this.ignoreUnknownElements;
    }
    
    public void setUnknownElementsIgnored(final boolean newValue) {
        this.ignoreUnknownElements = newValue;
    }
    
    public UnknownElementsListener getUnknownElementsListener() {
        return this.unknownElemsListener;
    }
    
    public void setUnknownElementsListener(final UnknownElementsListener listener) {
        this.unknownElemsListener = listener;
    }
}
