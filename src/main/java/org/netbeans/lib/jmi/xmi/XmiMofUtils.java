/*
 *                 Sun Public License Notice
 * 
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 * 
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2001 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.lib.jmi.xmi;

import java.util.*;
import javax.jmi.reflect.*;
import javax.jmi.model.*;

import org.w3c.dom.*;

import org.netbeans.api.mdr.*;

import org.netbeans.lib.jmi.util.*;

/**
 *
 * @author  msedliak
 * @version 
 */
public abstract class XmiMofUtils extends XmiUtils implements XmiConstants {


    
    /** Creates new BootReaderUtils */
    public XmiMofUtils() {
    }

    public static StructuralFeature getMetaFeature(String attributeName, RefClass proxyClass) {
        StructuralFeature result = null;
        Object metaObject = null;
        MofClass metaProxyClass = (MofClass)proxyClass.refMetaObject();
        try {
            metaObject = metaProxyClass.lookupElementExtended( getShortName( attributeName ) );
            if ( metaObject instanceof StructuralFeature ) result = (StructuralFeature)metaObject;
        } catch (javax.jmi.model.NameNotFoundException e) {
            //Logger.getDefault().log("Feature not found : "+attributeName);
        }
        return result;
    }

    public static void showAllAttributes(Collection attributes) {
        Logger.getDefault().log("Attributes - length : "+ attributes.size());
        for (Iterator it = attributes.iterator(); it.hasNext(); ) {
            Object att = it.next();
            if (att instanceof ModelElement)
                Logger.getDefault().log("Attributes : "+((ModelElement)att).getName()  );
            else
                if (att != null) 
                    Logger.getDefault().log("Attributes : "+att + " - of class : " +att.getClass() );
                else
                    Logger.getDefault().log("Attributes : NULLLLLLLLL " );
        }
    }

    public static String showCollectionAsString(Collection attributes, String separator) {
        String message = null;
        for (Iterator it = attributes.iterator(); it.hasNext(); ) {
            Object att = it.next();
            message = message+separator+message;
        }
        message = message.substring( message.indexOf( separator )+separator.length() );
        return message;
    }

    public static Collection getAllSupertypesContainedObjects(RefClass proxyClass) {
        ArrayList result = new ArrayList();
        ArrayList allContainedObjects = new ArrayList();
        MofClass metaProxyClass = (MofClass)proxyClass.refMetaObject();
        List superClasses = metaProxyClass.allSupertypes();
        Namespace namespace = null;
        for (Iterator it = superClasses.iterator(); it.hasNext(); ) {
            namespace = (Namespace)it.next();
            allContainedObjects.addAll( namespace.getContents() );
        }
        allContainedObjects.addAll( metaProxyClass.getContents() );;
        for (Iterator it = allContainedObjects.iterator(); it.hasNext(); ) {
            RefObject refObject = (RefObject)it.next();
            if ( (refObject instanceof Attribute) && ( !((Attribute)refObject).isDerived() ) ) result.add( refObject );
        }
        allContainedObjects.clear();
        allContainedObjects = null;
        return result;
    }

    public static Reference getReferenceMetaFeature(String referenceNodeName, RefClass proxyClass) {
        Reference result = null;
        StructuralFeature metaFeature = getMetaFeature(referenceNodeName, proxyClass);
        if (metaFeature instanceof Reference) result = (Reference)metaFeature;
        return result;
    }

    public static Attribute getAttributeMetaFeature(String referenceNodeName, RefClass proxyClass) {
        Attribute result = null;
        StructuralFeature metaFeature = getMetaFeature(referenceNodeName, proxyClass);
        if (metaFeature instanceof Attribute) result = (Attribute)metaFeature;
        return result;
    }
}
