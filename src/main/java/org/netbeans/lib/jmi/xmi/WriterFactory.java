// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIOutputConfig;
import org.netbeans.api.xmi.XMIWriter;
import org.netbeans.api.xmi.XMIWriterFactory;

public class WriterFactory extends XMIWriterFactory
{
    public XMIWriter createXMIWriter() {
        return new DelegatingWriter();
    }
    
    public XMIWriter createXMIWriter(final XMIOutputConfig configuration) {
        return new DelegatingWriter(configuration);
    }
}
