// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIInputConfig;
import org.netbeans.api.xmi.sax.XMIConsumer;
import org.netbeans.api.xmi.sax.XMIConsumerFactory;

public class ConsumerFactory extends XMIConsumerFactory
{
    public XMIConsumer createXMIConsumer() {
        return new Consumer();
    }
    
    public XMIConsumer createXMIConsumer(final XMIInputConfig configuration) {
        return new Consumer(configuration);
    }
}
