// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import java.io.InputStream;

public interface XMIHeaderConsumer
{
    void consumeHeader(final InputStream p0);
}
