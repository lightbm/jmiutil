// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import java.net.URL;
import java.io.IOException;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import org.netbeans.lib.jmi.util.Logger;
import javax.jmi.xmi.MalformedXMIException;
import java.util.Collection;
import javax.jmi.reflect.RefPackage;
import java.io.InputStream;
import org.netbeans.api.xmi.XMIInputConfig;
import org.netbeans.api.xmi.XMIReader;

public class SAXReader extends XMIReader
{
    private XMIInputConfig config;
    
    public SAXReader() {
        this(null);
    }
    
    public SAXReader(final XMIInputConfig cfg) {
        this.config = new InputConfig();
        if (cfg != null) {
            this.config.setReferenceResolver(cfg.getReferenceResolver());
            if (cfg instanceof InputConfig) {
                ((InputConfig)this.config).setHeaderConsumer(((InputConfig)cfg).getHeaderConsumer());
            }
        }
    }
    
    public XMIInputConfig getConfiguration() {
        return this.config;
    }
    
    public Collection read(final InputStream stream, final String uri, final RefPackage extent) throws IOException, MalformedXMIException {
        try {
            return new XmiSAXReader(this.config).read(stream, uri, new RefPackage[] { extent }, null);
        }
        catch (ParserConfigurationException e) {
            final MalformedXMIException ne = new MalformedXMIException(e.toString());
            Logger.getDefault().annotate((Throwable)ne, (Throwable)e);
            throw ne;
        }
        catch (SAXException e2) {
            final MalformedXMIException ne = new MalformedXMIException(e2.toString());
            Logger.getDefault().annotate((Throwable)ne, (Throwable)e2);
            throw ne;
        }
    }
    
    public Collection read(final String uri, final RefPackage extent) throws IOException, MalformedXMIException {
        return this.read(new URL(uri), new RefPackage[] { extent }, null);
    }
    
    private Collection read(final URL url, final RefPackage[] extents, final String encoding) throws IOException, MalformedXMIException {
        try {
            return new XmiSAXReader(this.config).read(url, extents, encoding);
        }
        catch (ParserConfigurationException e) {
            final MalformedXMIException ne = new MalformedXMIException(e.toString());
            Logger.getDefault().annotate((Throwable)ne, (Throwable)e);
            throw ne;
        }
        catch (SAXException e2) {
            final MalformedXMIException ne = new MalformedXMIException(e2.toString());
            Logger.getDefault().annotate((Throwable)ne, (Throwable)e2);
            throw ne;
        }
    }
}
