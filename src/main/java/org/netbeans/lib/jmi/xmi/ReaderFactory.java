// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIInputConfig;
import org.netbeans.api.xmi.XMIReader;
import org.netbeans.api.xmi.XMIReaderFactory;

public class ReaderFactory extends XMIReaderFactory
{
    public XMIReader createXMIReader() {
        return new SAXReader();
    }
    
    public XMIReader createXMIReader(final XMIInputConfig configuration) {
        return new SAXReader(configuration);
    }
}
