// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.xmi;

import org.netbeans.api.xmi.XMIReferenceProvider;
import org.netbeans.api.xmi.XMIOutputConfig;

public class OutputConfig extends XMIOutputConfig
{
    private XMIReferenceProvider provider;
    private XMIHeaderProvider headerProvider;
    private String encoding;
    
    public OutputConfig() {
        this.provider = null;
        this.headerProvider = null;
        this.encoding = null;
    }
    
    public void setReferenceProvider(final XMIReferenceProvider provider) {
        this.provider = provider;
    }
    
    public XMIReferenceProvider getReferenceProvider() {
        return this.provider;
    }
    
    public void setHeaderProvider(final XMIHeaderProvider headerProvider) {
        this.headerProvider = headerProvider;
    }
    
    public XMIHeaderProvider getHeaderProvider() {
        return this.headerProvider;
    }
    
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }
    
    public String getEncoding() {
        return this.encoding;
    }
}
