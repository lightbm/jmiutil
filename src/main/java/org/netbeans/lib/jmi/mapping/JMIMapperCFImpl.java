// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.mapping;

import java.io.IOException;
import javax.jmi.reflect.RefBaseObject;
import org.netbeans.api.mdr.JMIStreamFactory;
import java.io.Serializable;
import org.netbeans.api.mdr.JMIMapper;

public class JMIMapperCFImpl extends JMIMapper implements Serializable
{
    public void generate(final JMIStreamFactory sf, final RefBaseObject object) throws IOException {
        new ClassFileMapper(sf).visitRefBaseObject(object);
    }
}
