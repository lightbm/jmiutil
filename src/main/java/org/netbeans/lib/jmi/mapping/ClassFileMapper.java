/*
 *                 Sun Public License Notice
 *
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 *
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2001 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.lib.jmi.mapping;

import org.netbeans.api.mdr.JMIStreamFactory;
import org.netbeans.lib.jmi.util.ClassFileGenerator;
import org.netbeans.lib.jmi.util.ContainsIterator;
import javax.jmi.model.*;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author  Dusan Balek
 * @version 0.1
 */
public class ClassFileMapper extends GenericMapper {
    protected String[] getAncestorNames(GeneralizableElement ge, String postfix, String defaultAncestor) {
        Collection supertypes = ge.getSupertypes();
        if (supertypes.size() == 0)
            return new String [] {defaultAncestor};
            String[] ancestors = new String [supertypes.size()];
            int i = 0;
            for (Iterator it = supertypes.iterator(); it.hasNext(); i++)
                ancestors[i] = tagProvider.getTypeFullName((ModelElement) it.next()) + postfix;
            return ancestors;
    }
    
    protected static Object getTypedValue(String typeName, String value) {
        if (value == null)
            return null;
        else if (typeName.equals("String")) //NOI18N
            return value;
        else if (typeName.equals("Integer")) //NOI18N
            return new Integer(value);
        else if (typeName.equals("Boolean")) //NOI18N
            return new Integer(value.equalsIgnoreCase("true") ? 1 : 0); //NOI18N
        else if (typeName.equals("Double")) //NOI18N
            return new Double(value);
        else if (typeName.equals("Float")) //NOI18N
            return new Float(value);
        else if (typeName.equals("Long")) //NOI18N
            return new Long(value);
        else
            return null;
    }

    public ClassFileMapper(JMIStreamFactory sf) {
        this.generator = sf;
    }
    
    private final JMIStreamFactory generator;
    private OutputStream stream;

    protected boolean createStream(List pkg, String fileName) throws IOException {
        if (stream != null) {
            closeStream();
            throw new IllegalStateException("Attempting to create stream before previous stream was closed.");
        }
        stream = generator.createStream(pkg, fileName, JMIStreamFactory.EXT_CLASS);
        return stream != null;
    }
    
    protected void closeStream() throws IOException {
        if (stream == null)
            throw new IllegalStateException("Attempting to close the stream without opening it first.");
        OutputStream os = stream;
        stream = null;
        os.close();
    }
    
    protected void classProxyTemplate(javax.jmi.model.MofClass objClass) throws IOException {
        new ClassProxyGenerator(objClass).generate();
    }
    
    protected void classInstanceTemplate(javax.jmi.model.MofClass objClass) throws IOException {
        new ClassInstanceGenerator(objClass).generate();
    }
    
    // generates interface for a given association
    protected void associationTemplate(Association objAssociation) throws IOException {
        new AssociationGenerator(objAssociation).generate();
    }
    
    // generates interface for a given package
    protected void packageTemplate(MofPackage objPackage) throws IOException {
        new PackageGenerator(objPackage).generate();
    }
    
    //generates class for an exception
    protected void exceptionTemplate(MofException objException) throws IOException {
        new ExceptionGenerator(objException).generate();
    }
    
    //generates interface for an enumeration
    protected void enumerationInterfaceTemplate(EnumerationType objEnumeration) throws IOException {
        new EnumerationGenerator(objEnumeration).generate();
    }
    
    //generates class for an enumeration
    protected void enumerationClassTemplate(EnumerationType objEnumeration) throws IOException {
        new EnumerationImplGenerator(objEnumeration).generate();
    }
    
    // generates interface for a structure
    protected void structureTemplate(StructureType objStructure) throws IOException {
        new StructureGenerator(objStructure).generate();
    }
    
    abstract class JMIClassFileGenerator extends ClassFileGenerator {
        protected JMIClassFileGenerator(String className, String[] interfaces, String superclass, int accessFlags) {
            super(className, interfaces, superclass, accessFlags);
        }
        
        protected void generate() throws IOException {
            generateClassFile(stream);
        }
    }
    
    class ClassProxyGenerator extends JMIClassFileGenerator {
        
        protected javax.jmi.model.MofClass objClass;
        
        public ClassProxyGenerator(javax.jmi.model.MofClass objClass) {
            super(tagProvider.getTypeFullName(objClass) + CLASS_POSTFIX, new String[] {DT_CLASS}, DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT);
            this.objClass = objClass;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            for (Iterator it = objClass.getContents().iterator(); it.hasNext();) {
                ModelElement element = (ModelElement) it.next();
                if (element instanceof StructureType) {
                    ArrayList parameters = new ArrayList();
                    for (Iterator itt = ((StructureType)element).getContents().iterator(); itt.hasNext();) {
                        ModelElement field = (ModelElement) itt.next();
                        if (field instanceof StructureField)
                            parameters.add(getTypeName2((StructureField) field));
                    }
                    methods.add(new MethodInfo("create" + firstUpper(tagProvider.getSubstName(element)), getMethodDescriptor((String[])parameters.toArray(new String[parameters.size()]), tagProvider.getTypeFullName(element)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                }
            }
            ArrayList allAttributes =  new ArrayList();
            for (Iterator it = new ContainsIterator(objClass); it.hasNext();) {
                ModelElement element = (ModelElement) it.next();
                if (element instanceof Attribute) {
                    Attribute attr = (Attribute) element;
                    if (attr.getScope().equals(ScopeKindEnum.CLASSIFIER_LEVEL)) {
                        String attrName = firstUpper(tagProvider.getSubstName(attr));
                        Classifier attrType = getAttrType(attr);
                        String attrTypeName = getTypeName(attrType);
                        if (attr.getMultiplicity().getUpper() == 1) {
                            String name;
                            String setterName;
                            if (attrType instanceof PrimitiveType && attrType.getName().equals("Boolean")) { //NOI18N
                                if (attrName.substring(0, 2).equals("Is")) name = firstLower(attrName); //NOI18N
                                else name = "is" + attrName; //NOI18N
                                setterName = "set" + name.substring(2); //NOI18N
                            } else {
                                name = "get" + attrName; //NOI18N
                                setterName = "set" + attrName; //NOI18N
                            }
                            String getterType = attrTypeName;
                            if (attr.getMultiplicity().getLower() == 1)
                                getterType = getPrimitiveName(getterType);
                            methods.add(new MethodInfo(name, getMethodDescriptor(new String[0], getterType), ACC_PUBLIC | ACC_ABSTRACT));
                            if (attr.isChangeable())
                                methods.add(new MethodInfo(setterName, getMethodDescriptor(new String[] {getterType}, "void"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                        } else if (attr.getMultiplicity().getUpper() != 0)
                            methods.add(new MethodInfo("get" + attrName, getMethodDescriptor(new String[0], (attr.getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED)),  ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                    } else if (!attr.isDerived())
                        allAttributes.add(getTypeName(attr));
                } else if (element instanceof Operation) {
                    Operation oper = (Operation) element;
                    if (oper.getScope().equals(ScopeKindEnum.CLASSIFIER_LEVEL)) {
                        Collection parameters = new ArrayList();
                        String operType = "void"; //NOI18N
                        for (Iterator itt = oper.getContents().iterator(); itt.hasNext();) {
                            ModelElement el = (ModelElement) itt.next();
                            if (el instanceof Parameter) {
                                Parameter param = (Parameter) el;
                                if (param.getDirection().equals(DirectionKindEnum.RETURN_DIR))
                                    operType = getTypeName(param);
                                else
                                    parameters.add(getTypeName(param) + (param.getDirection().equals(DirectionKindEnum.IN_DIR) ? "" : "[]")); //NOI18N
                            }
                        }
                        MethodInfo mInfo = new MethodInfo(tagProvider.getSubstName(oper), getMethodDescriptor((String[])parameters.toArray(new String [parameters.size()]), operType),  ACC_PUBLIC | ACC_ABSTRACT);
                        Collection exceptions = oper.getExceptions();
                        short[] declaredExceptions = new short[exceptions.size()];
                        int i = 0;
                        for (Iterator itt = exceptions.iterator(); itt.hasNext(); i++)
                            declaredExceptions[i] = cp.getClass(dotToSlash(tagProvider.getTypeFullName((ModelElement)itt.next())));
                        mInfo.setDeclaredExceptions(declaredExceptions);
                        methods.add(mInfo);
                    }
                }
            }
            if (!objClass.isAbstract()) {
                methods.add(new MethodInfo("create" + tagProvider.getSubstName(objClass), getMethodDescriptor(new String[0], tagProvider.getTypeFullName(objClass)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                if (allAttributes.size() > 0)
                    methods.add(new MethodInfo("create" + tagProvider.getSubstName(objClass), getMethodDescriptor((String[])allAttributes.toArray(new String [allAttributes.size()]), tagProvider.getTypeFullName(objClass)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
            }
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            return new FieldInfo[0];
        }
    }
    
    class ClassInstanceGenerator extends JMIClassFileGenerator {
        
        protected javax.jmi.model.MofClass objClass;
        
        public ClassInstanceGenerator(javax.jmi.model.MofClass objClass) {
            super(tagProvider.getTypeFullName(objClass), getAncestorNames(objClass, "", DT_INSTANCE), DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT); //NOI18N
            this.objClass = objClass;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            for (Iterator it = objClass.getContents().iterator(); it.hasNext();) {
                ModelElement element = (ModelElement) it.next();
                if (element instanceof Feature) {
                    Feature feature = (Feature) element;
                    if (feature.getVisibility().equals(VisibilityKindEnum.PUBLIC_VIS)) {
                        if (feature instanceof Attribute) {
                            Attribute attr = (Attribute) feature;
                            String attrName = firstUpper(tagProvider.getSubstName(attr));
                            Classifier attrType = getAttrType(attr);
                            String attrTypeName = getTypeName(attrType);
                            if (attr.getMultiplicity().getUpper() == 1) {
                                String name;
                                String setterName;
                                if (attrType instanceof PrimitiveType && attrType.getName().equals("Boolean")) { //NOI18N
                                    if (attrName.substring(0, 2).equals("Is")) name = firstLower(attrName); //NOI18N
                                    else name = "is" + attrName; //NOI18N
                                    setterName = "set" + name.substring(2); //NOI18N
                                } else {
                                    name = "get" + attrName; //NOI18N
                                    setterName = "set" + attrName; //NOI18N
                                }
                                String getterType = attrTypeName;
                                if (attr.getMultiplicity().getLower() == 1)
                                    getterType = getPrimitiveName(getterType);
                                methods.add(new MethodInfo(name, getMethodDescriptor(new String[0], getterType), ACC_PUBLIC | ACC_ABSTRACT));
                                if (attr.isChangeable())
                                    methods.add(new MethodInfo(setterName, getMethodDescriptor(new String[] {getterType}, "void"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                            } else if (attr.getMultiplicity().getUpper() != 0)
                                methods.add(new MethodInfo("get" + attrName, getMethodDescriptor(new String[0], (attr.getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                        } else if (feature instanceof Operation) {
                            Operation oper = (Operation) feature;
                            Collection parameters = new ArrayList();
                            String operType = "void"; //NOI18N
                            for (Iterator itt = oper.getContents().iterator(); itt.hasNext();) {
                                ModelElement el = (ModelElement) itt.next();
                                if (el instanceof Parameter) {
                                    Parameter param = (Parameter) el;
                                    if (param.getDirection().equals(DirectionKindEnum.RETURN_DIR))
                                        operType = getTypeName(param);
                                    else
                                        parameters.add(getTypeName(param) + (param.getDirection().equals(DirectionKindEnum.IN_DIR) ? "" : "[]")); //NOI18N
                                }
                            }
                            MethodInfo mInfo = new MethodInfo(tagProvider.getSubstName(oper), getMethodDescriptor((String[])parameters.toArray(new String [parameters.size()]), operType), ACC_PUBLIC | ACC_ABSTRACT);
                            Collection exceptions = oper.getExceptions();
                            short[] declaredExceptions = new short[exceptions.size()];
                            int i = 0;
                            for (Iterator itt = exceptions.iterator(); itt.hasNext(); i++)
                                declaredExceptions[i] = cp.getClass(dotToSlash(tagProvider.getTypeFullName((ModelElement)itt.next())));
                            mInfo.setDeclaredExceptions(declaredExceptions);
                            methods.add(mInfo);
                        } else if (feature instanceof Reference) {
                            Reference ref = (Reference) feature;
                            String refName = firstUpper(tagProvider.getSubstName(ref));
                            String refType = getTypeName(getAttrType(ref));
                            if (ref.getMultiplicity().getUpper() == 1) {
                                methods.add(new MethodInfo("get" + refName, getMethodDescriptor(new String[0], refType), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                                if (ref.isChangeable())
                                    methods.add(new MethodInfo("set" + refName, getMethodDescriptor(new String[] {refType}, "void"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                            } else if (ref.getMultiplicity().getUpper() != 0) {
                                methods.add(new MethodInfo("get" + refName, getMethodDescriptor(new String[0], (ref.getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                            }
                        }
                    }
                }
            }
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            ArrayList fields = new ArrayList();
            for (Iterator it = objClass.getContents().iterator(); it.hasNext();) {
                ModelElement element = (ModelElement) it.next();
                if (element instanceof Constant) {
                    DataType type = (DataType) getAttrType((Constant)element);
                    String value = ((Constant)element).getValue();
                    FieldInfo fInfo = new FieldInfo(tagProvider.getSubstName(element), getFieldType(getTypeName2((Constant)element)), ACC_PUBLIC | ACC_STATIC | ACC_FINAL);
                    fInfo.setConstValue(getTypedValue(type.getName(), value));
                    fields.add(fInfo);
                }
            }
            return (FieldInfo[]) fields.toArray(new FieldInfo[fields.size()]);
        }
    }
    
    class AssociationGenerator extends JMIClassFileGenerator {
        
        protected Association objAssociation;
        
        public AssociationGenerator(Association objAssociation) {
            super(tagProvider.getTypeFullName(objAssociation), new String[] {DT_ASSOCIATION}, DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT);
            this.objAssociation = objAssociation;
            
            
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            AssociationEnd[] ends = new AssociationEnd[2];
            int i = 0;
            for (Iterator it = objAssociation.getContents().iterator(); it.hasNext() && i < 2;) {
                Object element = it.next();
                if (element instanceof AssociationEnd)
                    ends[i++] = (AssociationEnd) element;
            }
            String end1Name = tagProvider.getSubstName(ends[0]);
            String end1Class = tagProvider.getTypeFullName(getAttrType(ends[0]));
            String end2Name = tagProvider.getSubstName(ends[1]);
            String end2Class = tagProvider.getTypeFullName(getAttrType(ends[1]));
            // exists(, )
            methods.add(new MethodInfo("exists", getMethodDescriptor(new String[] {end1Class, end2Class}, "boolean"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
            // ()
            if (ends[0].isNavigable()) {
                methods.add(new MethodInfo("get" + firstUpper(end1Name), getMethodDescriptor(new String[] {end2Class}, //NOI18N
                ends[0].getMultiplicity().getUpper() == 1 ? end1Class : (ends[0].getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED)), ACC_PUBLIC | ACC_ABSTRACT));
            }
            // ()
            if (ends[1].isNavigable()) {
                methods.add(new MethodInfo("get" + firstUpper(end2Name), getMethodDescriptor(new String[] {end1Class}, //NOI18N
                ends[1].getMultiplicity().getUpper() == 1 ? end2Class : (ends[1].getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED)), ACC_PUBLIC | ACC_ABSTRACT));
            }
            if (ends[0].isChangeable() && ends[1].isChangeable()) {
                // add(, )
                methods.add(new MethodInfo("add", getMethodDescriptor(new String[] {end1Class, end2Class}, "boolean"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                // remove(, )
                methods.add(new MethodInfo("remove", getMethodDescriptor(new String[] {end1Class, end2Class}, "boolean"), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
            }
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            return new FieldInfo[0];
        }
    }
    
    class PackageGenerator extends JMIClassFileGenerator {
        
        protected MofPackage objPackage;
        
        public PackageGenerator(MofPackage objPackage) {
            super(tagProvider.getTypeFullName(objPackage) + PACKAGE_POSTFIX, getAncestorNames(objPackage, PACKAGE_POSTFIX, DT_PACKAGE), DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT);
            this.objPackage = objPackage;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            for (Iterator it = objPackage.getContents().iterator(); it.hasNext();) {
                ModelElement element = (ModelElement) it.next();
                String typeName = tagProvider.getTypeFullName(element);
                String elementName = tagProvider.getSubstName(element);
                if (element instanceof Association) {
                } else if (element instanceof javax.jmi.model.MofClass) {
                    typeName += CLASS_POSTFIX;
                } else if (element instanceof MofPackage) {
                    typeName += PACKAGE_POSTFIX;
                } else if (element instanceof EnumerationType) {
                    continue;
                } else if (element instanceof StructureType) {
                    ArrayList parameters = new ArrayList();
                    for (Iterator itt = ((StructureType)element).getContents().iterator(); itt.hasNext();) {
                        ModelElement field = (ModelElement) itt.next();
                        if (field instanceof StructureField)
                            parameters.add(getTypeName2((StructureField) field));
                    }
                    methods.add(new MethodInfo("create" + firstUpper(tagProvider.getSubstName(element)), getMethodDescriptor((String[])parameters.toArray(new String[parameters.size()]), tagProvider.getTypeFullName(element)), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                    continue;
                } else if (element instanceof Import) {
                    Import imp = (Import) element;
                    if (imp.isClustered() && imp.getVisibility().equals(VisibilityKindEnum.PUBLIC_VIS)) {
                        Namespace namespace = imp.getImportedNamespace();
                        if (namespace instanceof MofPackage) {
                            typeName = tagProvider.getTypeFullName(namespace) + PACKAGE_POSTFIX;
                            element = namespace;
                        }
                    } else 
                        continue;
                } else
                    continue;
                if (((GeneralizableElement) element).getVisibility().equals(VisibilityKindEnum.PUBLIC_VIS)) {
                    methods.add(new MethodInfo("get" + elementName, getMethodDescriptor(new String[0], typeName), ACC_PUBLIC | ACC_ABSTRACT)); //NOI18N
                }
            }
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            return new FieldInfo[] {};
        }
    }
    
    class ExceptionGenerator extends JMIClassFileGenerator {
        
        protected MofException objException;
        
        public ExceptionGenerator(MofException objException) {
            super(tagProvider.getTypeFullName(objException), new String[0], DT_EXCEPTION, ACC_PUBLIC | ACC_SUPER);
            this.objException = objException;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            ArrayList paramInits = new ArrayList();
            ArrayList paramTypes = new ArrayList();
            // parameter accessors
            short i = 1;            
            for (Iterator it = objException.getContents().iterator(); it.hasNext();) {
                Object element = it.next();
                if (element instanceof Parameter) {
                    Parameter param = (Parameter) element;
                    String typeName = getTypeName(param);
                    paramTypes.add(typeName);
                    String getterName = firstLower(tagProvider.getSubstName(param));
                    String paramName = removeUnderscores(getterName);
                    if (param.getType() instanceof PrimitiveType && param.getType().getName().equals("Boolean")) { //NOI18N
                        if (getterName.indexOf("is") != 0) //NOI18N
                            getterName = removeUnderscores("is_" + getterName); //NOI18N
                    } else
                        getterName = removeUnderscores("get_" + getterName); //NOI18N
                    MethodInfo mInfo = new MethodInfo(getterName, getMethodDescriptor(new String[0], typeName), ACC_PUBLIC);
                    DataOutputStream code = new DataOutputStream(mInfo.getCodeStream());
                    codeReturnFieldValue(className, paramName, getFieldType(typeName), false, code);
                    mInfo.setMaxStack((short)1);
                    mInfo.setMaxLocals((short)1);
                    methods.add(mInfo);
                    ByteArrayOutputStream bout = new ByteArrayOutputStream(6);
                    DataOutputStream paramInitCode = new DataOutputStream(bout);
                    code_aload(0, paramInitCode);
                    code_aload(i++, paramInitCode);
                    paramInitCode.writeByte(opc_putfield);
                    paramInitCode.writeShort(cp.getFieldRef(dotToSlash(className), paramName, getFieldType(typeName)));
                    paramInits.add(bout.toByteArray());
                    //                getterName = paramName + ": \" + " + paramName;
                    //                if (codeMsg.length() == 0) {
                    //                    codeMsg.append("\"" + getterName);
                    //                } else {
                    //                    codeMsg.append(" + \", " + getterName);
                }
            }
            //constructor
            MethodInfo mInfo = new MethodInfo("", getMethodDescriptor((String[])paramTypes.toArray(new String[paramTypes.size()]), "void"), ACC_PUBLIC); //NOI18N
            DataOutputStream code = new DataOutputStream(mInfo.getCodeStream());
//            generate("super(" + codeMsg.toString() + ");");
            code_aload(0, code);
            code.writeByte(opc_invokespecial);
            code.writeShort(cp.getMethodRef(dotToSlash(className), "", getMethodDescriptor(new String[0], "void"))); //NOI18N
            for (Iterator it = paramInits.iterator(); it.hasNext();)
                code.write((byte[]) it.next());
            code.writeByte(opc_return);
            mInfo.setMaxStack((short)2);
            mInfo.setMaxLocals(i);
            methods.add(mInfo);
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            ArrayList fields = new ArrayList();
            // private parameter holders
            for (Iterator it = objException.getContents().iterator(); it.hasNext();) {
                Object element = it.next();
                if (element instanceof Parameter) {
                    Parameter param = (Parameter) element;
                    String typeName = getTypeName(param);
                    String paramName = removeUnderscores(firstLower(tagProvider.getSubstName(param)));
                    fields.add(new FieldInfo(paramName, getFieldType(typeName), ACC_PRIVATE | ACC_FINAL));
                }
            }
            return (FieldInfo[]) fields.toArray(new FieldInfo[fields.size()]);
        }
    }
    
    class EnumerationGenerator extends JMIClassFileGenerator {
        
        protected EnumerationType objEnumeration;
        
        public EnumerationGenerator(EnumerationType objEnumeration) {
            super(tagProvider.getTypeFullName(objEnumeration), new String[] {DT_ENUMERATION}, DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT);
            this.objEnumeration = objEnumeration;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            return new MethodInfo[0];
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            return new FieldInfo[0];
        }
    }
    
    class EnumerationImplGenerator extends JMIClassFileGenerator {
        
        protected EnumerationType objEnumeration;
        
        public EnumerationImplGenerator(EnumerationType objEnumeration) {
            super(tagProvider.getTypeFullName(objEnumeration) + ENUM_POSTFIX, new String[] {tagProvider.getTypeFullName(objEnumeration)}, DT_ANY, ACC_PUBLIC | ACC_FINAL | ACC_SUPER);
            this.objEnumeration = objEnumeration;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            // constructor
            MethodInfo mInfo = new MethodInfo("", getMethodDescriptor(new String[] {"java.lang.String"}, "void"), ACC_PRIVATE); //NOI18N
            DataOutputStream code = new DataOutputStream(mInfo.getCodeStream());
            code_aload(0, code);
            code.writeByte(opc_invokespecial);
            code.writeShort(cp.getMethodRef("java/lang/Object", "", getMethodDescriptor(new String[0], "void"))); //NOI18N
            code_aload(0, code);
            code_aload(1, code);
            code.writeByte(opc_putfield);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "literalName", getFieldType("java.lang.String"))); //NOI18N
            code.writeByte(opc_return);
            mInfo.setMaxStack((short)2);
            mInfo.setMaxLocals((short)2);
            methods.add(mInfo);
            // refTypeName
            mInfo = new MethodInfo("refTypeName", getMethodDescriptor(new String[0], "java.util.List"), ACC_PUBLIC); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            codeReturnFieldValue(className, "typeName", getFieldType("java.util.List"), false, code); //NOI18N
            mInfo.setMaxStack((short)1);
            mInfo.setMaxLocals((short)1);
            methods.add(mInfo);
            // toString
            mInfo = new MethodInfo("toString", getMethodDescriptor(new String[0], "java.lang.String"), ACC_PUBLIC); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            codeReturnFieldValue(className, "literalName", getFieldType("java.lang.String"), false, code); //NOI18N
            mInfo.setMaxStack((short)1);
            mInfo.setMaxLocals((short)1);
            methods.add(mInfo);
            // hashCode
            mInfo = new MethodInfo("hashCode", getMethodDescriptor(new String[0], "int"), ACC_PUBLIC); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            code_aload(0, code);
            code.writeByte(opc_getfield);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "literalName", getFieldType("java.lang.String"))); //NOI18N
            code.write(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Object", "hashCode", getMethodDescriptor(new String[0], "int"))); //NOI18N
            code.writeByte(opc_ireturn);
            mInfo.setMaxStack((short)1);
            mInfo.setMaxLocals((short)1);
            methods.add(mInfo);
            // equals
            mInfo = new MethodInfo("equals", getMethodDescriptor(new String[] {"java.lang.Object"}, "boolean"), ACC_PUBLIC); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            // if (o instanceof Enum) return (o == this);
            code_aload(1, code);
            code.writeByte(opc_instanceof);
            code.writeShort(cp.getClass(dotToSlash(className)));
            code.writeByte(opc_ifeq);
            code.writeShort(12);
            code.writeByte(opc_aload_0 + 1);
            code.writeByte(opc_aload_0);
            code.writeByte(opc_if_acmpne);
            code.writeShort(5);
            code.writeByte(opc_iconst_1);
            code.writeByte(opc_ireturn);
            code.writeByte(opc_iconst_0);
            code.writeByte(opc_ireturn);
            // else if (o instanceof )
            //     return (o.toString().equals(literalName));
            code_aload(1, code);
            code.writeByte(opc_instanceof);
            code.writeShort(cp.getClass(dotToSlash(tagProvider.getTypeFullName(objEnumeration))));
            code.writeByte(opc_ifeq);
            code.writeShort(15);
            code.writeByte(opc_aload_0 + 1);
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Object", "toString", getMethodDescriptor(new String[0], "java.lang.String"))); //NOI18N
            code.writeByte(opc_aload_0);
            code.writeByte(opc_getfield);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "literalName", getFieldType("java.lang.String"))); //NOI18N
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Object", "equals", getMethodDescriptor(new String[] {"java.lang.Object"}, "boolean"))); //NOI18N
            code.writeByte(opc_ireturn);
            // else return ((o instanceof javax.jmi.reflect.RefEnum)
            //     && ((javax.jmi.reflect.RefEnum) o).refTypeName().equals(typeName)
            //     && o.toString().equals(literalName));
            code_aload(1, code);
            code.writeByte(opc_instanceof);
            code.writeShort(cp.getClass(dotToSlash(DT_ENUMERATION)));
            code.writeByte(opc_ifeq);
            code.writeShort(39);
            code.writeByte(opc_aload_0 + 1);
            code.writeByte(opc_checkcast);
            code.writeShort(cp.getClass(dotToSlash(DT_ENUMERATION)));
            code.writeByte(opc_invokeinterface);
            code.writeShort(cp.getInterfaceMethodRef(dotToSlash(DT_ENUMERATION), "refTypeName", getMethodDescriptor(new String[0], "java.util.List"))); //NOI18N
            code.writeByte(1);
            code.writeByte(0);
            code.writeByte(opc_getstatic);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "typeName", getFieldType("java.util.List"))); //NOI18N
            code.writeByte(opc_invokeinterface);
            code.writeShort(cp.getInterfaceMethodRef("java/lang/Object", "equals", getMethodDescriptor(new String[] {"java.lang.Object"}, "boolean"))); //NOI18N
            code.writeByte(2);
            code.writeByte(0);
            code.writeByte(opc_ifeq);
            code.writeShort(19);
            code.writeByte(opc_aload_0 + 1);
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Object", "toString", getMethodDescriptor(new String[0], "java.lang.String"))); //NOI18N
            code.writeByte(opc_aload_0);
            code.writeByte(opc_getfield);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "literalName", getFieldType("java.lang.String"))); //NOI18N
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Object", "equals", getMethodDescriptor(new String[] {"java.lang.Object"}, "boolean"))); //NOI18N
            code.writeByte(opc_ifeq);
            code.writeShort(5);
            code.writeByte(opc_iconst_1);
            code.writeByte(opc_ireturn);
            code.writeByte(opc_iconst_0);
            code.writeByte(opc_ireturn);
            mInfo.setMaxStack((short)2);
            mInfo.setMaxLocals((short)2);
            methods.add(mInfo);
            // readResolve
            mInfo = new MethodInfo("readResolve", getMethodDescriptor(new String[0], "java.lang.Object"), ACC_PROTECTED); //NOI18N
            mInfo.setDeclaredExceptions(new short[] {cp.getClass("java/io/ObjectStreamException")}); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            code_aload(0, code);
            code.write(opc_getfield);
            code.writeShort(cp.getFieldRef(dotToSlash(className), "literalName", getFieldType("java.lang.String"))); //NOI18N
            code.writeByte(opc_invokestatic);
            code.writeShort(cp.getMethodRef(dotToSlash(className), "forName", getMethodDescriptor(new String[] {"java.lang.String"}, tagProvider.getTypeFullName(objEnumeration)))); //NOI18N
            code.writeByte(opc_areturn);
            code_astore(1, code);
            code.writeByte(opc_new);
            code.writeShort(cp.getClass("java/io/ObjectStreamException")); //NOI18N
            code.writeByte(opc_dup);
            code_aload(1, code);
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/Throwable", "getMessage", getMethodDescriptor(new String[0], "java.lang.String"))); //NOI18N
            code.writeByte(opc_invokespecial);
            code.writeShort(cp.getMethodRef("java/io/ObjectStreamException", "", getMethodDescriptor(new String[] {"java.lang.String"}, "void"))); //NOI18N
            code.writeByte(opc_athrow);
            mInfo.getExceptionTable().add(new ExceptionTableEntry((short) 0, (short) 8, (short) 8, cp.getClass("java/lang/IllegalArgumentException"))); //NOI18N
            mInfo.setMaxStack((short)3);
            mInfo.setMaxLocals((short)2);
            methods.add(mInfo);
            // forName & static initializer
            mInfo = new MethodInfo("forName", getMethodDescriptor(new String[] {"java.lang.String"}, tagProvider.getTypeFullName(objEnumeration)), ACC_PUBLIC | ACC_STATIC); //NOI18N
            code = new DataOutputStream(mInfo.getCodeStream());
            MethodInfo initInfo = new MethodInfo("", getMethodDescriptor(new String[0], "void"), ACC_STATIC); //NOI18N
            DataOutputStream initCode = new DataOutputStream(initInfo.getCodeStream());
            for (Iterator it = objEnumeration.getLabels().iterator(); it.hasNext();) {
                String literal = (String) it.next();
                code_aload(0, code);
                code_ldc(cp.getString(literal), code);
                code.writeByte(opc_invokevirtual);
                code.writeShort(cp.getMethodRef("java/lang/String", "equals", getMethodDescriptor(new String[] {"java.lang.Object"}, "boolean"))); //NOI18N
                code.writeByte(opc_ifeq);
                code.writeShort(7);
                code.writeByte(opc_getstatic);
                code.writeShort(cp.getFieldRef(dotToSlash(className), tagProvider.mapEnumLiteral(literal), getFieldType(className)));
                code.write(opc_areturn);
                initCode.writeByte(opc_new);
                initCode.writeShort(cp.getClass(dotToSlash(className)));
                initCode.writeByte(opc_dup);
                code_ldc(cp.getString(literal), initCode);
                initCode.writeByte(opc_invokespecial);
                initCode.writeShort(cp.getMethodRef(dotToSlash(className), "", getMethodDescriptor(new String[] {"java.lang.String"}, "void"))); //NOI18N
                initCode.writeByte(opc_putstatic);
                initCode.writeShort(cp.getFieldRef(dotToSlash(className), tagProvider.mapEnumLiteral(literal), getFieldType(className)));
            }
            code.writeByte(opc_new);
            code.writeShort(cp.getClass("java/lang/IllegalArgumentException")); //NOI18N
            code.writeByte(opc_dup);
            code.writeByte(opc_new);
            code.writeShort(cp.getClass("java/lang/StringBuffer")); //NOI18N
            code.writeByte(opc_dup);
            code.writeByte(opc_invokespecial);
            code.writeShort(cp.getMethodRef("java/lang/StringBuffer", "", getMethodDescriptor(new String[0], "void"))); //NOI18N
            code_ldc(cp.getString("Unknown enumeration value "), code); //NOI18N
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/StringBuffer", "append", getMethodDescriptor(new String[] {"java.lang.String"}, "java.lang.StringBuffer"))); //NOI18N
            code_aload(0, code);
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/StringBuffer", "append", getMethodDescriptor(new String[] {"java.lang.String"}, "java.lang.StringBuffer"))); //NOI18N
            code_ldc(cp.getString(" for type " + tagProvider.getTypeFullName(objEnumeration)), code); //NOI18N
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/StringBuffer", "append", getMethodDescriptor(new String[] {"java.lang.String"}, "java.lang.StringBuffer"))); //NOI18N
            code.writeByte(opc_invokevirtual);
            code.writeShort(cp.getMethodRef("java/lang/StringBuffer", "toString", getMethodDescriptor(new String[0], "java.lang.String"))); //NOI18N
            code.writeByte(opc_invokespecial);
            code.writeShort(cp.getMethodRef("java/lang/IllegalArgumentException", "", getMethodDescriptor(new String[] {"java.lang.String"}, "void"))); //NOI18N
            code.writeByte(opc_athrow);
            mInfo.setMaxStack((short)4);
            mInfo.setMaxLocals((short)1);
            methods.add(mInfo);
            initCode.writeByte(opc_new);
            initCode.writeShort(cp.getClass("java/util/ArrayList")); //NOI18N
            initCode.writeByte(opc_dup);
            initCode.writeByte(opc_invokespecial);
            initCode.writeShort(cp.getMethodRef("java/util/ArrayList", "", getMethodDescriptor(new String[0], "void"))); //NOI18N
            code_astore(0, initCode);
            for (Iterator it = objEnumeration.getQualifiedName().iterator(); it.hasNext();) {
                code_aload(0, initCode);
                code_ldc(cp.getString((String)it.next()), initCode);
                initCode.writeByte(opc_invokevirtual);
                initCode.writeShort(cp.getMethodRef("java/util/ArrayList", "add", getMethodDescriptor(new String [] {"java.lang.Object"}, "boolean"))); //NOI18N
                initCode.writeByte(opc_pop);
            }
            code_aload(0, initCode);
            initCode.writeByte(opc_invokestatic);
            initCode.writeShort(cp.getMethodRef("java/util/Collections", "unmodifiableList", getMethodDescriptor(new String[] {"java.util.List"}, "java.util.List"))); //NOI18N
            initCode.writeByte(opc_putstatic);
            initCode.writeShort(cp.getFieldRef(dotToSlash(className), "typeName", getFieldType("java.util.List"))); //NOI18N
            initCode.writeByte(opc_return);
            initInfo.setMaxStack((short)3);
            initInfo.setMaxLocals((short)1);
            methods.add(initInfo);
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            ArrayList fields = new ArrayList();
            // public fields (literals)
            for (Iterator it = objEnumeration.getLabels().iterator(); it.hasNext();) {
                String literal = (String) it.next();
                FieldInfo fInfo = new FieldInfo(tagProvider.mapEnumLiteral(literal), getFieldType(className), ACC_PUBLIC | ACC_STATIC | ACC_FINAL);
                fields.add(fInfo);
            }
            // private fields
            fields.add(new FieldInfo("typeName", getFieldType("java.util.List"), ACC_PRIVATE | ACC_STATIC | ACC_FINAL)); //NOI18N
            fields.add(new FieldInfo("literalName", getFieldType("java.lang.String"), ACC_PRIVATE | ACC_FINAL)); //NOI18N
            return (FieldInfo[]) fields.toArray(new FieldInfo[fields.size()]);
        }
    }
    
    class StructureGenerator extends JMIClassFileGenerator {
        
        protected StructureType objStructure;
        
        public StructureGenerator(StructureType objStructure) {
            super(tagProvider.getTypeFullName(objStructure), new String[] {DT_STRUCTURE}, DT_ANY, ACC_PUBLIC | ACC_INTERFACE | ACC_ABSTRACT);
            this.objStructure = objStructure;
        }
        
        protected MethodInfo[] generateMethods() throws IOException {
            ArrayList methods = new ArrayList();
            // attribute accessors
            for (Iterator it = objStructure.getContents().iterator(); it.hasNext();) {
                ModelElement field = (ModelElement) it.next();
                if (field instanceof StructureField) {
                    Classifier fieldType = ((StructureField) field).getType();
                    String memberName = firstUpper(tagProvider.getSubstName(field));
                    if (fieldType instanceof PrimitiveType && fieldType.getName().equals("Boolean")) { //NOI18N
                        if (memberName.indexOf("Is") != 0) //NOI18N
                            memberName = "is" + memberName; //NOI18N
                        else
                            memberName = firstLower(memberName);
                    } else
                        memberName = "get" + memberName; //NOI18N
                    MethodInfo mInfo = new MethodInfo(memberName, getMethodDescriptor(new String[0], getPrimitiveName(getTypeName(fieldType))), ACC_PUBLIC | ACC_ABSTRACT);
//                    mInfo.setDeclaredExceptions(new short[] {cp.getClass("javax/jmi/reflect/JmiException")});
                    methods.add(mInfo);
                }
            }
            return (MethodInfo[]) methods.toArray(new MethodInfo[methods.size()]);
        }
        
        protected FieldInfo[] generateFields() throws IOException {
            return new FieldInfo[0];
        }
    }
}
