// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.mapping;

import java.beans.IntrospectionException;
import org.openide.util.NbBundle;
import java.beans.PropertyDescriptor;
import java.beans.BeanDescriptor;
import java.beans.MethodDescriptor;
import java.beans.EventSetDescriptor;
import java.beans.SimpleBeanInfo;

public class JMIMapperImplBeanInfo extends SimpleBeanInfo
{
    private static final int PROPERTY_header = 0;
    private static EventSetDescriptor[] eventSets;
    private static MethodDescriptor[] methods;
    private static final int defaultPropertyIndex = -1;
    private static final int defaultEventIndex = -1;
    
    private static BeanDescriptor getBdescriptor() {
        final BeanDescriptor beanDescriptor = new BeanDescriptor(JMIMapperImpl.class, null);
        return beanDescriptor;
    }
    
    private static PropertyDescriptor[] getPdescriptor() {
        final PropertyDescriptor[] properties = { null };
        try {
            (properties[0] = new PropertyDescriptor("header", JMIMapperImpl.class, "getHeader", "setHeader")).setDisplayName(NbBundle.getMessage(JMIMapperImpl.class, "PROP_HEADER"));
            properties[0].setShortDescription(NbBundle.getMessage(JMIMapperImpl.class, "HINT_HEADER"));
        }
        catch (IntrospectionException ex) {}
        return properties;
    }
    
    private static EventSetDescriptor[] getEdescriptor() {
        return JMIMapperImplBeanInfo.eventSets;
    }
    
    private static MethodDescriptor[] getMdescriptor() {
        return JMIMapperImplBeanInfo.methods;
    }
    
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    public int getDefaultPropertyIndex() {
        return -1;
    }
    
    public int getDefaultEventIndex() {
        return -1;
    }
    
    static {
        JMIMapperImplBeanInfo.eventSets = null;
        JMIMapperImplBeanInfo.methods = null;
    }
}
