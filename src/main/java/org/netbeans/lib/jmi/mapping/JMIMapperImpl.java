// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.mapping;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import javax.jmi.reflect.RefBaseObject;
import org.netbeans.api.mdr.JMIStreamFactory;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import org.netbeans.api.mdr.JMIMapper;

public class JMIMapperImpl extends JMIMapper implements Serializable
{
    public static final String PROP_HEADER = "header";
    private transient PropertyChangeSupport supp;
    private String header;
    
    public JMIMapperImpl() {
        this.header = "";
    }
    
    public String getHeader() {
        return this.header;
    }
    
    public void setHeader(final String header) {
        final String oldHeader = this.header;
        if (!oldHeader.equals(header)) {
            this.header = header;
            this.init();
            this.supp.firePropertyChange("header", oldHeader, header);
        }
    }
    
    public void generate(final JMIStreamFactory sf, final RefBaseObject object) throws IOException {
        new JavaMapper(sf, this.header).visitRefBaseObject(object);
    }
    
    public final void addPropertyChangeListener(final PropertyChangeListener l) {
        this.init();
        this.supp.addPropertyChangeListener(l);
    }
    
    public final void removePropertyChangeListener(final PropertyChangeListener l) {
        if (this.supp != null) {
            this.supp.removePropertyChangeListener(l);
        }
    }
    
    private synchronized void init() {
        if (this.supp == null) {
            this.supp = new PropertyChangeSupport(this);
        }
    }
}
