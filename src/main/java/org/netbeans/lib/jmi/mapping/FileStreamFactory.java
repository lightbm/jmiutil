/*
 *                 Sun Public License Notice
 *
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 *
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2002 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.lib.jmi.mapping;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import org.netbeans.api.mdr.JMIStreamFactory;

/**
 *
 * @author  Martin Matula
 */
public class FileStreamFactory extends JMIStreamFactory {
    private final File targetDir;
    private final long modelLastModified;
    
    /** Creates a new instance of FileStreamFactory. Using this constructor is
     *  equivalent to using FileStreamFactory(targetDir, null).
     *  @see #FileStreamFactory(File, java.util.Date)
     *  @param targetDir The target directory
     *  @throws IllegalArgumentException If targetDir == null,
     *          if the target directory does not already exist, or if the
     *          target file is not a directory.
     */
    public FileStreamFactory(File targetDir) {
        this(targetDir, null);
    }
    
    /** Creates a new instance of FileStreamFactory. FileStreamFactory will
     *  compare the last modified date of any existing target files against
     *  the value of the modelLastModified paramter. If the target
     *  file's last modified date is later than modelLastModified/code>
     *  then {@link #createStream(List, String, String)} will return
     *  null to prevent the {@link JMIMapper} from overwriting
     *  the file. If modelLastModified == null, then
     *  createStream will never return null.
     *
     *  @param targetDir The target directory
     *  @param modelLastModified The date that the MOF model was last changed,
     *                           or null.
     *  @throws IllegalArgumentException If targetDir == null,
     *          if the target directory does not already exist, or if the
     *          target file is not a directory.
     */
    public FileStreamFactory(File targetDir, java.util.Date modelLastModified) {
        if (targetDir == null)
            throw new IllegalArgumentException("ERROR: targetDir is null");
        else if (!targetDir.exists())
            throw new IllegalArgumentException("ERROR: targetDir does not exist");
        else if (!targetDir.isDirectory()) {
            throw new IllegalArgumentException("ERROR: targetDir has to be a directory");
        }
        this.targetDir = targetDir;
        this.modelLastModified = modelLastModified != null ? modelLastModified.getTime() : 0L;
    }
    
    public OutputStream createStream(List pkg, String className, String extension) throws IOException {
        File current = targetDir;
        for (Iterator it = pkg.iterator(); it.hasNext();) {
            current = new File(current, (String) it.next());
            if (!current.exists()) {
                current.mkdir();
            }
        }
        return createStream(new File(current, className + "." + extension));
    }

    /** This method is called by {@link #createStream(List,String,String)} to
     *  really create the stream. It return creates the FileOutputSream
     *  corresponding to the given file if upToDate is false; otherwise it
     *  returns null. This is a convenient place for subclasses to hook
     *  into the stream creation process for logging or checking different
     *  criteria besides just the file's last modified date.
     */
    protected OutputStream createStream(java.io.File file) throws IOException {
        return modelLastModified == 0 || modelLastModified > file.lastModified()
            ? new FileOutputStream(file) : null;
    }
    
}
