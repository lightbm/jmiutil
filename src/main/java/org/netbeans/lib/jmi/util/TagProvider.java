/*
 *                 Sun Public License Notice
 *
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 *
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2001 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.lib.jmi.util;

import java.util.*;

import javax.jmi.model.*;

/**
 *
 * @author  mmatula
 * @version
 */
public class TagProvider extends Object {
    public static final String TAGID_PACKAGE_PREFIX = "javax.jmi.packagePrefix"; //NOI18N
    public static final String TAGID_SUBSTITUTE_NAME = "javax.jmi.substituteName"; //NOI18N
    public static final String TAGID_IGNORE_LIFECYCLE = "javax.jmi.ignoreLifecycle"; //NOI18N
    public static final String TAGID_IMPL_PACKAGE_PREFIX = "org.netbeans.implPackagePrefix"; //NOI18N
    public static final String TAGID_XMI_NAMESPACE = "org.omg.xmi.namespace"; //NOI18N

    public static final int ASSOCIATION = 0;
    public static final int INSTANCE = 1;
    public static final int CLASS = 2;
    public static final int PACKAGE = 3;
    
    private static final String DT_ORDERED = "java.util.List"; //NOI18N
    private static final String DT_MULTIVALUED = "java.util.Collection"; //NOI18N

    private final HashMap valueCache = new HashMap();

    /** Creates new TagProvider */
    public TagProvider() {
    }

    public Tag getTag(ModelElement element, String tagID) {
        Tag tag = null;

        Collection tags = ((ModelPackage) element.refImmediatePackage()).getAttachesTo().getTag(element);
        Tag temp;
        for (Iterator it = tags.iterator(); it.hasNext();) {
            temp = (Tag) it.next();
            if (tagID.equals(temp.getTagId())) {
                tag = temp;
                break;
            }
        }

        return tag;
    }

    public Collection getTagValues(ModelElement element, String tagID) {
        Tag tag = getTag(element, tagID);

        if (tag == null) {
            return null;
        }
        else {
            return tag.getValues();
        }
    }

    public String getTagValue(ModelElement element, String tagID) {
        if (element == null) return null;
        String tagKey = element.refMofId() + ":" + tagID; //NOI18N
        String value = (String) valueCache.get(tagKey);

        if (value == null) {
            Collection values = getTagValues(element, tagID);

            if (values != null && values.size() > 0) {
                value = (String) values.iterator().next();
                valueCache.put(tagKey, value);
            }
        }

        return value;
    }

    public String getTagValue(ModelElement element, String tagID, String defaultValue) {
        String result = getTagValue(element, tagID);
        if (result == null) {
            result = defaultValue;
        }

        return result;
    }

    public String getTypeFullName(ModelElement type) {
        return getTypePrefix(type, new StringBuffer(50)).append('.').append(getSubstName(type)).toString();
    }

    public String getDataTypeName(Parameter prm) {
        MultiplicityType mp = prm.getMultiplicity();

        if (mp.getUpper() > 1 || mp.getUpper() == -1) {
            return (mp.isOrdered() ? DT_ORDERED : DT_MULTIVALUED);
        } else {
            return getDataTypeName(prm.getType());
        }
    }

    public String getDataTypeName(Classifier type) {
        String result;

        if (type instanceof PrimitiveType) {
            result = "java.lang." + type.getName(); //NOI18N
        } else if (type instanceof AliasType) {
            result = getDataTypeName(((AliasType) type).getType());
        } else if (type instanceof CollectionType) {
            result = (((CollectionType) type).getMultiplicity().isOrdered() ? DT_ORDERED : DT_MULTIVALUED);
        } else {
            result = getTypeFullName(type);
        }

        return result;
    }

    public String getSubstName(ModelElement element) {
        String result = getTagValue(element, TAGID_SUBSTITUTE_NAME);
        if (result == null)
            result = element.getName();
        if (element instanceof Constant) {
            result = mapName (result, true, false);
        } else {
            boolean flag = element instanceof MofClass || element instanceof MofPackage ||
                element instanceof Association || element instanceof MofException ||
                element instanceof StructureType || element instanceof EnumerationType ||
                element instanceof CollectionType || element instanceof Import;
            result = mapName (result, false, !flag);
        } // else
        if (element instanceof MofException && !result.endsWith("Exception")) //NOI18N
            result = result + "Exception"; //NOI18N
        return result;
    }

    private static String mapName(String name, boolean toLiteral, boolean firstLower) {
        StringBuffer buffer = new StringBuffer (32);
        boolean wordRead = false;
        boolean lowerCharDetected = false;
        for (int x = 0; x < name.length (); x++) {
            char c = name.charAt (x);
            if (c == '-' || c == '_' || Character.isWhitespace (c)) {
                if (wordRead) {
                    // first whitespace following end of word reached
                    if (toLiteral)
                        buffer.append ('_');
                    wordRead = false;
                    lowerCharDetected = false;
                }
            } else {
                if (lowerCharDetected && Character.isUpperCase (c)) {
                    // a next word started ...
                    if (toLiteral)
                        buffer.append ('_');
                    wordRead = false;
                    lowerCharDetected = false;
                } else if (Character.isLowerCase (c))
                    lowerCharDetected = true;
                if (!wordRead || toLiteral)
                    buffer.append (Character.toUpperCase (c));
                else
                    buffer.append (Character.toLowerCase (c));
                wordRead = true;
            } // if
        } // for
        if (buffer.length () > 0) {
            if (toLiteral && !wordRead)
                buffer.deleteCharAt (buffer.length () - 1);
            if (firstLower)
                buffer.replace (0, 1, new String (new char [] {(Character.toLowerCase (buffer.charAt(0)))}));
        }
        return buffer.toString ();
    }

    public static String mapEnumLiteral (String name) {
        return mapName (name, true, false);
    }

    public String getNamespaceName(MofPackage type) {
        String result = getTagValue(type, TAGID_XMI_NAMESPACE);
        if (result == null) {
            result = type.getName();
        }
        return result;
    }

    public String getTypePrefix(ModelElement metaObject) {
        return getTypePrefix(metaObject, new StringBuffer(50)).toString();
    }

    public String getImplFullName(ModelElement object, int type) {
        return getImplPrefix(object, new StringBuffer(50)).append('.').append(getSubstName(object)).append(type == CLASS ? "Class" : (type == PACKAGE ? "Package" : "")).append("Impl").toString(); //NOI18N
    }
    
    private StringBuffer getImplPrefix(ModelElement object, StringBuffer sb) {
        try {
            ModelElement pckg = object;

            while (!(pckg instanceof MofPackage)) {
                pckg = pckg.getContainer();
            }
            
            Namespace container = pckg.getContainer();
            if (container == null) {
                String result = getTagValue(pckg, TAGID_IMPL_PACKAGE_PREFIX);
                if (result == null) {
                    result = getTagValue(pckg, TAGID_PACKAGE_PREFIX);
                    if (result == null) {
                        sb.append("impl"); //NOI18N
                    } else {
                        result = result.toLowerCase();
                        if ("javax.jmi".equals(result)) { //NOI18N
                            sb.append("org.netbeans.jmiimpl.mof"); //NOI18N
                        } else if (result.startsWith("org.netbeans.jmi.")) { //NOI18N
                            sb.append("org.netbeans.jmiimpl").append(result.substring(16)); //NOI18N
                        } else if (result.startsWith("org.omg")) { //NOI18N
                            if (result.length() == 7) {
                                sb.append("org.netbeans.jmiimpl.omg"); //NOI18N
                            } else if (result.charAt(7) == '.') {
                                sb.append("org.netbeans.jmiimpl.omg").append(result.substring(7)); //NOI18N
                            } else {
                                sb.append(result).append(".impl"); //NOI18N
                            }
                        } else {
                            sb.append(result).append(".impl"); //NOI18N
                        }
                    }
                } else {
                    sb.append(result.toLowerCase());
                }
            } else {
                getImplPrefix(container, sb);
            }

            String packageName = getTagValue(pckg, TAGID_SUBSTITUTE_NAME);
            if (packageName == null) {
                packageName = mapName(pckg.getName(), false, true);
            }

            sb.append('.').append(packageName.toLowerCase());
            
            return sb;
        } catch (Exception e) {
            throw (DebugException) Logger.getDefault().annotate(new DebugException(), e);
        }
    }

    private StringBuffer getTypePrefix(ModelElement metaObject, StringBuffer sb) {
        ModelElement pckg = metaObject;

        while (!(pckg instanceof MofPackage)) {
            pckg = pckg.getContainer();
        }

        Namespace container = pckg.getContainer();
        if (container == null) {
            String result = getTagValue(pckg, TagProvider.TAGID_PACKAGE_PREFIX);
            if (result != null) {
                sb.append(result.toLowerCase()).append('.');
            }
        } else {
            getTypePrefix(container, sb).append('.');
        }

        String packageName = getTagValue(pckg, TagProvider.TAGID_SUBSTITUTE_NAME);
        if (packageName == null) {
            packageName = mapName(pckg.getName(), false, true);
        }

        return sb.append(packageName.toLowerCase());
    }
}
