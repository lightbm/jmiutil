/*
 *                 Sun Public License Notice
 *
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 *
 * The Original Code is NetBeans. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1997-2002 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
package org.netbeans.lib.jmi.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import javax.jmi.model.ModelPackage;
import javax.jmi.model.MofPackage;
import javax.jmi.model.Tag;
import javax.jmi.reflect.RefPackage;
import javax.jmi.xmi.XmiReader;
import org.netbeans.api.mdr.MDRepository;
import org.openide.util.Lookup;

/**
 *
 * @author Martin Matula
 */
public class MetamodelManager {
    protected final MDRepository repository;
    private final Class cls;
    private Map cache;
    private ModelPackage extent;
    private String extentName;
    private String version;
    private String fileName;
    
    /** Creates a new instance of MetamodelManager */
    public MetamodelManager(MDRepository repository, Class cls) {
        this.repository = repository;
        this.cls = cls;
    }
    
    public synchronized MofPackage findRootPackage(String packageName) {
        if (extent == null) {
            update();
        }        
        if (cache == null) {
            cache = new HashMap();
            for (Iterator it = extent.getMofPackage().refAllOfClass().iterator(); it.hasNext();) {
                MofPackage pkg = (MofPackage) it.next();
                if (pkg.getContainer() == null) {
                    cache.put(pkg.getName(), pkg);
                }                        
            }
        }
        
        return (MofPackage) cache.get(packageName);
    }
    
    protected synchronized String getVersion() {
        initCheck();
        return version;
    }
    
    protected synchronized String getExtentName() {
        initCheck();
        return extentName;
    }
    
    private void initCheck() {
        if (fileName == null) {
            try {
                Manifest mf = new Manifest(cls.getResourceAsStream("/META-INF/MANIFEST.MF"));
                Attributes attrs = mf.getMainAttributes();
                extentName = attrs.getValue("NBMDR-Metamodel");
                fileName = "/" + attrs.getValue("NBMDR-Metamodel-File");
                version = attrs.getValue("NBMDR-Metamodel-Version");
            } catch (java.io.IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }
    
    private void update() {
        initCheck();
        boolean fail = true;
        repository.beginTrans(true);
        try {

            // [PENDING] should check whether the package is really instance of MOF
            Logger.getDefault().log("Looking for extent " + extentName + " in repository " + repository);
            extent = (ModelPackage) repository.getExtent(extentName);
            
            if (extent != null) {
                // extent was found -> check if it contains correct versions of packages
                Tag tag = null;
                for (Iterator it = extent.getTag().refAllOfClass().iterator(); it.hasNext();) {
                    Tag temp = (Tag) it.next();
                    if ("org.netbeans.version".equals(temp.getTagId())) {
                        tag = temp;
                        break;
                    }
                }
                // if the version tag was not found, or the version is incorrect,
                // reinstall the model
                if (tag == null || tag.getValues().isEmpty() || 
                    !tag.getValues().iterator().next().equals(version)) {
                    Logger.getDefault().log("Metamodel version is outdated - it needs to be reloaded.");
                    String names[] = repository.getExtentNames();
                    for (int i = 0; i < names.length; i++) {
                        RefPackage tmp = repository.getExtent(names[i]);
                        if (!(tmp instanceof ModelPackage)) {
                            tmp.refDelete();
                        }
                    }
                    extent.refDelete();
                    extent = null;
                }
            } else {
                Logger.getDefault().log("Extent not found.");
            }

            if (extent == null) {
                // extent needs to be created
                extent = (ModelPackage) repository.createExtent(extentName);
                XmiReader xmr = (XmiReader) Lookup.getDefault().lookup(XmiReader.class);
                Collection outermostElements = xmr.read(cls.getResource(fileName).toString(), extent);
                extent.getTag().createTag("org.netbeans.version", "", "org.netbeans.version", Arrays.asList(new String[] {version}));
                // cache outermost packages
                cache = new HashMap();
                for (Iterator it = outermostElements.iterator(); it.hasNext();) {
                    Object temp = it.next();
                    if (temp instanceof MofPackage) {
                        cache.put(((MofPackage) temp).getName(), temp);
                    }
                }
            }
            fail = false;
        } catch (Exception ex) {
            throw (IllegalStateException) Logger.getDefault().annotate(new IllegalStateException("Metamodel XMI malformed."), ex);
        } finally {
            repository.endTrans(fail);
        }
    }
}
