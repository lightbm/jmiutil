// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.io.ByteArrayOutputStream;
import java.util.Locale;
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.OutputStream;

@SuppressWarnings("unchecked")
public abstract class ClassFileGenerator
{
    public static final int JAVA_MAGIC = -889275714;
    public static final int JAVA_DEFAULT_VERSION = 45;
    public static final int JAVA_DEFAULT_MINOR_VERSION = 3;
    public static final int CONSTANT_UTF8 = 1;
    public static final int CONSTANT_INTEGER = 3;
    public static final int CONSTANT_FLOAT = 4;
    public static final int CONSTANT_LONG = 5;
    public static final int CONSTANT_DOUBLE = 6;
    public static final int CONSTANT_CLASS = 7;
    public static final int CONSTANT_STRING = 8;
    public static final int CONSTANT_FIELD = 9;
    public static final int CONSTANT_METHOD = 10;
    public static final int CONSTANT_INTERFACEMETHOD = 11;
    public static final int CONSTANT_NAMEANDTYPE = 12;
    public static final int ACC_PUBLIC = 1;
    public static final int ACC_PRIVATE = 2;
    public static final int ACC_PROTECTED = 4;
    public static final int ACC_STATIC = 8;
    public static final int ACC_FINAL = 16;
    public static final int ACC_SUPER = 32;
    public static final int ACC_INTERFACE = 512;
    public static final int ACC_ABSTRACT = 1024;
    public static final int T_BYTE = 8;
    public static final int opc_aconst_null = 1;
    public static final int opc_iconst_0 = 3;
    public static final int opc_iconst_1 = 4;
    public static final int opc_lconst_0 = 9;
    public static final int opc_fconst_0 = 11;
    public static final int opc_dconst_0 = 14;
    public static final int opc_bipush = 16;
    public static final int opc_sipush = 17;
    public static final int opc_ldc = 18;
    public static final int opc_ldc_w = 19;
    public static final int opc_iload = 21;
    public static final int opc_lload = 22;
    public static final int opc_fload = 23;
    public static final int opc_dload = 24;
    public static final int opc_aload = 25;
    public static final int opc_iload_0 = 26;
    public static final int opc_lload_0 = 30;
    public static final int opc_fload_0 = 34;
    public static final int opc_dload_0 = 38;
    public static final int opc_aload_0 = 42;
    public static final int opc_aload_1 = 43;
    public static final int opc_aaload = 50;
    public static final int opc_istore = 54;
    public static final int opc_lstore = 55;
    public static final int opc_fstore = 56;
    public static final int opc_dstore = 57;
    public static final int opc_astore = 58;
    public static final int opc_istore_0 = 59;
    public static final int opc_lstore_0 = 63;
    public static final int opc_fstore_0 = 67;
    public static final int opc_dstore_0 = 71;
    public static final int opc_astore_0 = 75;
    public static final int opc_aastore = 83;
    public static final int opc_bastore = 84;
    public static final int opc_pop = 87;
    public static final int opc_dup = 89;
    public static final int opc_ifeq = 153;
    public static final int opc_ifne = 154;
    public static final int opc_ifle = 158;
    public static final int opc_if_icmpeq = 159;
    public static final int opc_if_acmpne = 166;
    public static final int opc_goto = 167;
    public static final int opc_jsr = 168;
    public static final int opc_ret = 169;
    public static final int opc_ireturn = 172;
    public static final int opc_lreturn = 173;
    public static final int opc_freturn = 174;
    public static final int opc_dreturn = 175;
    public static final int opc_areturn = 176;
    public static final int opc_return = 177;
    public static final int opc_getstatic = 178;
    public static final int opc_putstatic = 179;
    public static final int opc_getfield = 180;
    public static final int opc_putfield = 181;
    public static final int opc_invokevirtual = 182;
    public static final int opc_invokespecial = 183;
    public static final int opc_invokestatic = 184;
    public static final int opc_invokeinterface = 185;
    public static final int opc_new = 187;
    public static final int opc_newarray = 188;
    public static final int opc_anewarray = 189;
    public static final int opc_arraylength = 190;
    public static final int opc_athrow = 191;
    public static final int opc_checkcast = 192;
    public static final int opc_instanceof = 193;
    public static final int opc_wide = 196;
    public static final int opc_ifnull = 198;
    protected String className;
    protected int accessFlags;
    protected String superclassName;
    protected String[] ifaceNames;
    protected ConstantPool cp;
    
    protected ClassFileGenerator(final String className, final String[] interfaces, final String superclass, final int accessFlags) {
        this.cp = new ConstantPool();
        this.className = className;
        this.ifaceNames = interfaces;
        this.superclassName = superclass;
        this.accessFlags = accessFlags;
    }
    
    protected final void generateClassFile(final OutputStream stream) {
        try {
            final MethodInfo[] methods = this.generateMethods();
            final FieldInfo[] fields = this.generateFields();
            this.cp.getClass(dotToSlash(this.className));
            this.cp.getClass(dotToSlash(this.superclassName));
            for (int i = 0; i < this.ifaceNames.length; ++i) {
                this.cp.getClass(dotToSlash(this.ifaceNames[i]));
            }
            this.cp.setReadOnly();
            final DataOutputStream dout = new DataOutputStream(stream);
            dout.writeInt(-889275714);
            dout.writeShort(3);
            dout.writeShort(45);
            this.cp.write(dout);
            dout.writeShort(this.accessFlags);
            dout.writeShort(this.cp.getClass(dotToSlash(this.className)));
            dout.writeShort(this.cp.getClass(dotToSlash(this.superclassName)));
            dout.writeShort(this.ifaceNames.length);
            for (int j = 0; j < this.ifaceNames.length; ++j) {
                dout.writeShort(this.cp.getClass(dotToSlash(this.ifaceNames[j])));
            }
            dout.writeShort(fields.length);
            for (int j = 0; j < fields.length; ++j) {
                fields[j].write(dout);
            }
            dout.writeShort(methods.length);
            for (int j = 0; j < methods.length; ++j) {
                methods[j].write(dout);
            }
            dout.writeShort(0);
            dout.close();
        }
        catch (IOException e) {
            throw new InternalError("unexpected I/O Exception");
        }
    }
    
    protected abstract MethodInfo[] generateMethods() throws IOException;
    
    protected abstract FieldInfo[] generateFields() throws IOException;
    
    protected void code_iload(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 21, 26, out);
    }
    
    protected void code_lload(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 22, 30, out);
    }
    
    protected void code_fload(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 23, 34, out);
    }
    
    protected void code_dload(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 24, 38, out);
    }
    
    protected void code_aload(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 25, 42, out);
    }
    
    protected void code_istore(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 54, 59, out);
    }
    
    protected void code_lstore(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 55, 63, out);
    }
    
    protected void code_fstore(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 56, 67, out);
    }
    
    protected void code_dstore(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 57, 71, out);
    }
    
    protected void code_astore(final int lvar, final DataOutputStream out) throws IOException {
        this.codeLocalLoadStore(lvar, 58, 75, out);
    }
    
    protected void codeLocalLoadStore(final int lvar, final int opcode, final int opcode_0, final DataOutputStream out) throws IOException {
        _assert(lvar >= 0 && lvar <= 65535);
        if (lvar <= 3) {
            out.writeByte(opcode_0 + lvar);
        }
        else if (lvar <= 255) {
            out.writeByte(opcode);
            out.writeByte(lvar & 0xFF);
        }
        else {
            out.writeByte(196);
            out.writeByte(opcode);
            out.writeShort(lvar & 0xFFFF);
        }
    }
    
    protected void code_ldc(final int index, final DataOutputStream out) throws IOException {
        _assert(index >= 0 && index <= 65535);
        if (index <= 255) {
            out.writeByte(18);
            out.writeByte(index & 0xFF);
        }
        else {
            out.writeByte(19);
            out.writeShort(index & 0xFFFF);
        }
    }
    
    protected void code_ipush(final int value, final DataOutputStream out) throws IOException {
        if (value >= -1 && value <= 5) {
            out.writeByte(3 + value);
        }
        else if (value >= -128 && value <= 127) {
            out.writeByte(16);
            out.writeByte(value & 0xFF);
        }
        else if (value >= -32768 && value <= 32767) {
            out.writeByte(17);
            out.writeShort(value & 0xFFFF);
        }
        else {
            _assert(false);
        }
    }
    
    protected void codeReturnFieldValue(final String className, final String fieldName, final String descriptor, final boolean isStatic, final DataOutputStream out) throws IOException {
        final short index = this.cp.getFieldRef(dotToSlash(className), fieldName, descriptor);
        _assert(index >= 0);
        if (isStatic) {
            out.writeByte(178);
        }
        else {
            this.code_aload(0, out);
            out.writeByte(180);
        }
        out.writeShort(index);
        if (descriptor.equals("I") || descriptor.equals("S") || descriptor.equals("C") || descriptor.equals("B") || descriptor.equals("Z")) {
            out.writeByte(172);
        }
        else if (descriptor.equals("J")) {
            out.writeByte(173);
        }
        else if (descriptor.equals("F")) {
            out.writeByte(174);
        }
        else if (descriptor.equals("D")) {
            out.writeByte(175);
        }
        else {
            out.writeByte(176);
        }
    }
    
    protected void codeClassForName(final Class<?> cl, final DataOutputStream out) throws IOException {
        this.code_ldc(this.cp.getString(cl.getName()), out);
        out.writeByte(184);
        out.writeShort(this.cp.getMethodRef("java/lang/Class", "forName", "(Ljava/lang/String;)Ljava/lang/Class;"));
    }
    
    protected static String firstUpper(final String text) {
        try {
            return text.substring(0, 1).toUpperCase(Locale.US) + text.substring(1);
        }
        catch (IndexOutOfBoundsException e) {
            return "";
        }
    }
    
    protected static String firstLower(final String text) {
        try {
            return text.substring(0, 1).toLowerCase(Locale.US) + text.substring(1);
        }
        catch (IndexOutOfBoundsException e) {
            return "";
        }
    }
    
    protected static void _assert(final boolean assertion) {
        if (!assertion) {
            throw new InternalError("assertion failure");
        }
    }
    
    protected static String dotToSlash(final String name) {
        return name.replace('.', '/');
    }
    
    protected static String getMethodDescriptor(final String[] parameterTypeNames, final String returnTypeName) {
        return getParameterDescriptors(parameterTypeNames) + (returnTypeName.equals("void") ? "V" : getFieldType(returnTypeName));
    }
    
    protected static String getParameterDescriptors(final String[] parameterTypeNames) {
        final StringBuffer desc = new StringBuffer("(");
        for (int i = 0; i < parameterTypeNames.length; ++i) {
            desc.append(getFieldType(parameterTypeNames[i]));
        }
        desc.append(')');
        return desc.toString();
    }
    
    protected static String getFieldType(final String typeName) {
        final PrimitiveTypeInfo ptInfo = PrimitiveTypeInfo.get(typeName);
        if (ptInfo != null) {
            return ptInfo.baseTypeString;
        }
        if (typeName.endsWith("[]")) {
            return "[" + getFieldType(typeName.substring(0, typeName.length() - 2).trim());
        }
        return "L" + dotToSlash(typeName) + ";";
    }
    
    protected static int getWordsPerType(final String typeName) {
        if (typeName.equals("long") || typeName.equals("double")) {
            return 2;
        }
        return 1;
    }
    
    protected final class FieldInfo
    {
        private int accessFlags;
        private String name;
        private String descriptor;
        private Object constValue;
        
        public FieldInfo(final String name, final String descriptor, final int accessFlags) {
            this.name = name;
            this.descriptor = descriptor;
            this.accessFlags = accessFlags;
            ClassFileGenerator.this.cp.getUtf8(name);
            ClassFileGenerator.this.cp.getUtf8(descriptor);
        }
        
        public void setConstValue(final Object value) {
            if ((this.accessFlags & 0x8) != 0x0) {
                this.constValue = value;
                ClassFileGenerator.this.cp.getUtf8("ConstantValue");
                ClassFileGenerator.this.cp.getUnknownValue(this.constValue);
            }
        }
        
        public final void write(final DataOutputStream out) throws IOException {
            out.writeShort(this.accessFlags);
            out.writeShort(ClassFileGenerator.this.cp.getUtf8(this.name));
            out.writeShort(ClassFileGenerator.this.cp.getUtf8(this.descriptor));
            if (this.constValue == null) {
                out.writeShort(0);
            }
            else {
                out.writeShort(1);
                out.writeShort(ClassFileGenerator.this.cp.getUtf8("ConstantValue"));
                out.writeInt(2);
                out.writeShort(ClassFileGenerator.this.cp.getUnknownValue(this.constValue));
            }
        }
        
        public boolean equals(final Object o) {
            return o instanceof FieldInfo && ((FieldInfo)o).name.equalsIgnoreCase(this.name);
        }
        
        public int hashCode() {
            return this.name.toUpperCase(Locale.US).hashCode();
        }
    }
    
    protected static final class ExceptionTableEntry
    {
        public short startPc;
        public short endPc;
        public short handlerPc;
        public short catchType;
        
        public ExceptionTableEntry(final short startPc, final short endPc, final short handlerPc, final short catchType) {
            this.startPc = startPc;
            this.endPc = endPc;
            this.handlerPc = handlerPc;
            this.catchType = catchType;
        }
    }
    
    protected final class MethodInfo
    {
        private int accessFlags;
        private String name;
        private String descriptor;
        private ByteArrayOutputStream code;
        private short maxStack;
        private short maxLocals;
        private short[] declaredExceptions;
        private List<ExceptionTableEntry> exceptionTable;
        
        public MethodInfo(final String name, final String descriptor, final int accessFlags) {
            this.code = new ByteArrayOutputStream();
            this.exceptionTable = new ArrayList<>();
            this.name = name;
            this.descriptor = descriptor;
            this.accessFlags = accessFlags;
            ClassFileGenerator.this.cp.getUtf8(name);
            ClassFileGenerator.this.cp.getUtf8(descriptor);
        }
        
        public void setDeclaredExceptions(final short[] exceptions) {
            ClassFileGenerator.this.cp.getUtf8("Exceptions");
            this.declaredExceptions = exceptions;
        }
        
        public ByteArrayOutputStream getCodeStream() {
            ClassFileGenerator.this.cp.getUtf8("Code");
            return this.code;
        }
        
        public void setMaxStack(final short max) {
            this.maxStack = max;
        }
        
        public void setMaxLocals(final short max) {
            this.maxLocals = max;
        }
        
        public List<ExceptionTableEntry> getExceptionTable() {
            return this.exceptionTable;
        }
        
		public void write(final DataOutputStream out) throws IOException {
            out.writeShort(this.accessFlags);
            out.writeShort(ClassFileGenerator.this.cp.getUtf8(this.name));
            out.writeShort(ClassFileGenerator.this.cp.getUtf8(this.descriptor));
            short count = 0;
            if (this.code.size() > 0) {
                ++count;
            }
            if (this.declaredExceptions != null && this.declaredExceptions.length > 0) {
                ++count;
            }
            out.writeShort(count);
            if (this.code.size() > 0) {
                out.writeShort(ClassFileGenerator.this.cp.getUtf8("Code"));
                out.writeInt(12 + this.code.size() + 8 * this.exceptionTable.size());
                out.writeShort(this.maxStack);
                out.writeShort(this.maxLocals);
                out.writeInt(this.code.size());
                this.code.writeTo(out);
                out.writeShort(this.exceptionTable.size());
                for (final ExceptionTableEntry e : (Collection<ExceptionTableEntry>)this.exceptionTable) {
                    out.writeShort(e.startPc);
                    out.writeShort(e.endPc);
                    out.writeShort(e.handlerPc);
                    out.writeShort(e.catchType);
                }
                out.writeShort(0);
            }
            if (this.declaredExceptions != null && this.declaredExceptions.length > 0) {
                out.writeShort(ClassFileGenerator.this.cp.getUtf8("Exceptions"));
                out.writeInt(2 + 2 * this.declaredExceptions.length);
                out.writeShort(this.declaredExceptions.length);
                for (int i = 0; i < this.declaredExceptions.length; ++i) {
                    out.writeShort(this.declaredExceptions[i]);
                }
            }
        }
    }
    
    protected static class PrimitiveTypeInfo
    {
        public String baseTypeString;
        public String wrapperClassName;
        public String wrapperConstructorDesc;
        public String unwrapMethodName;
        public String unwrapMethodDesc;
        private static Map<String,PrimitiveTypeInfo> table;
        
        private PrimitiveTypeInfo(final String baseTypeString, final String wrapperClassName, final String wrapperConstructorDesc, final String unwrapMethodName, final String unwrapMethodDesc) {
            this.baseTypeString = baseTypeString;
            this.wrapperClassName = wrapperClassName;
            this.wrapperConstructorDesc = wrapperConstructorDesc;
            this.unwrapMethodName = unwrapMethodName;
            this.unwrapMethodDesc = unwrapMethodDesc;
        }
        
        public static PrimitiveTypeInfo get(final String name) {
            return PrimitiveTypeInfo.table.get(name);
        }
        
        static {
            (PrimitiveTypeInfo.table = new HashMap<>(11)).put("int", new PrimitiveTypeInfo("I", "java/lang/Integer", "(I)V", "intValue", "()I"));
            PrimitiveTypeInfo.table.put("boolean", new PrimitiveTypeInfo("Z", "java/lang/Boolean", "(Z)V", "booleanValue", "()Z"));
            PrimitiveTypeInfo.table.put("byte", new PrimitiveTypeInfo("B", "java/lang/Byte", "(B)V", "byteValue", "()B"));
            PrimitiveTypeInfo.table.put("char", new PrimitiveTypeInfo("C", "java/lang/Char", "(C)V", "charValue", "()C"));
            PrimitiveTypeInfo.table.put("short", new PrimitiveTypeInfo("S", "java/lang/Short", "(S)V", "shortValue", "()S"));
            PrimitiveTypeInfo.table.put("long", new PrimitiveTypeInfo("J", "java/lang/Long", "(J)V", "longValue", "()J"));
            PrimitiveTypeInfo.table.put("float", new PrimitiveTypeInfo("F", "java/lang/Float", "(F)V", "floatValue", "()F"));
            PrimitiveTypeInfo.table.put("double", new PrimitiveTypeInfo("D", "java/lang/Double", "(D)V", "doubleValue", "()D"));
        }
    }
    
    protected static class ConstantPool
    {
        private List<Entry> pool;
        private Map<Object,Short> map;
        private boolean readOnly;
        
        protected ConstantPool() {
            this.pool = new ArrayList<>(32);
            this.map = new HashMap<>(16);
            this.readOnly = false;
        }
        
        public short getUtf8(final String s) {
            if (s == null) {
                throw new NullPointerException();
            }
            return this.getValue(s);
        }
        
        public short getInteger(final int i) {
            return this.getValue(new Integer(i));
        }
        
        public short getFloat(final float f) {
            return this.getValue(new Float(f));
        }
        
        public short getLong(final long l) {
            return this.getValue(new Long(l));
        }
        
        public short getDouble(final double d) {
            return this.getValue(new Double(d));
        }
        
        public short getClass(final String name) {
            final short utf8Index = this.getUtf8(name);
            return this.getIndirect(new IndirectEntry(7, utf8Index));
        }
        
        public short getString(final String s) {
            final short utf8Index = this.getUtf8(s);
            return this.getIndirect(new IndirectEntry(8, utf8Index));
        }
        
        public short getFieldRef(final String className, final String name, final String descriptor) {
            final short classIndex = this.getClass(className);
            final short nameAndTypeIndex = this.getNameAndType(name, descriptor);
            return this.getIndirect(new IndirectEntry(9, classIndex, nameAndTypeIndex));
        }
        
        public short getMethodRef(final String className, final String name, final String descriptor) {
            final short classIndex = this.getClass(className);
            final short nameAndTypeIndex = this.getNameAndType(name, descriptor);
            return this.getIndirect(new IndirectEntry(10, classIndex, nameAndTypeIndex));
        }
        
        public short getInterfaceMethodRef(final String className, final String name, final String descriptor) {
            final short classIndex = this.getClass(className);
            final short nameAndTypeIndex = this.getNameAndType(name, descriptor);
            return this.getIndirect(new IndirectEntry(11, classIndex, nameAndTypeIndex));
        }
        
        public short getNameAndType(final String name, final String descriptor) {
            final short nameIndex = this.getUtf8(name);
            final short descriptorIndex = this.getUtf8(descriptor);
            return this.getIndirect(new IndirectEntry(12, nameIndex, descriptorIndex));
        }
        
        public short getUnknownValue(final Object value) {
            if (value == null) {
                throw new NullPointerException();
            }
            if (value instanceof String) {
                return this.getString((String)value);
            }
            if (value instanceof Integer || value instanceof Float || value instanceof Long || value instanceof Double) {
                return this.getValue(value);
            }
            throw new InternalError("bogus value entry: " + value);
        }
        
        public void setReadOnly() {
            this.readOnly = true;
        }
        
        public void write(final OutputStream out) throws IOException {
            final DataOutputStream dataOut = new DataOutputStream(out);
            dataOut.writeShort(this.pool.size() + 1);
            for (final Entry e : this.pool) {
                e.write(dataOut);
            }
        }
        
        private short addEntry(final Entry entry) {
            this.pool.add(entry);
            return (short)this.pool.size();
        }
        
        private short getValue(final Object key) {
            final Short index = this.map.get(key);
            if (index != null) {
                return index;
            }
            if (this.readOnly) {
                throw new InternalError("late constant pool addition: " + key);
            }
            final short i = this.addEntry(new ValueEntry(key));
            this.map.put(key, new Short(i));
            return i;
        }
        
        private short getIndirect(final IndirectEntry e) {
            final Short index = this.map.get(e);
            if (index != null) {
                return index;
            }
            if (this.readOnly) {
                throw new InternalError("late constant pool addition");
            }
            final short i = this.addEntry(e);
            this.map.put(e, new Short(i));
            return i;
        }
        
        private abstract static class Entry
        {
            public abstract void write(final DataOutputStream p0) throws IOException;
        }
        
        private static class ValueEntry extends Entry
        {
            private Object value;
            
            public ValueEntry(final Object value) {
                this.value = value;
            }
            
            public void write(final DataOutputStream out) throws IOException {
                if (this.value instanceof String) {
                    out.writeByte(1);
                    out.writeUTF((String)this.value);
                }
                else if (this.value instanceof Integer) {
                    out.writeByte(3);
                    out.writeInt((int)this.value);
                }
                else if (this.value instanceof Float) {
                    out.writeByte(4);
                    out.writeFloat((float)this.value);
                }
                else if (this.value instanceof Long) {
                    out.writeByte(5);
                    out.writeLong((long)this.value);
                }
                else {
                    if (!(this.value instanceof Double)) {
                        throw new InternalError("bogus value entry: " + this.value);
                    }
                    out.writeDouble(6.0);
                    out.writeDouble((double)this.value);
                }
            }
        }
        
        private static class IndirectEntry extends Entry
        {
            private int tag;
            private short index0;
            private short index1;
            
            public IndirectEntry(final int tag, final short index) {
                this.tag = tag;
                this.index0 = index;
                this.index1 = 0;
            }
            
            public IndirectEntry(final int tag, final short index0, final short index1) {
                this.tag = tag;
                this.index0 = index0;
                this.index1 = index1;
            }
            
            public void write(final DataOutputStream out) throws IOException {
                out.writeByte(this.tag);
                out.writeShort(this.index0);
                if (this.tag == 9 || this.tag == 10 || this.tag == 11 || this.tag == 12) {
                    out.writeShort(this.index1);
                }
            }
            
            public int hashCode() {
                return this.tag + this.index0 + this.index1;
            }
            
            public boolean equals(final Object obj) {
                if (obj instanceof IndirectEntry) {
                    final IndirectEntry other = (IndirectEntry)obj;
                    if (this.tag == other.tag && this.index0 == other.index0 && this.index1 == other.index1) {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
