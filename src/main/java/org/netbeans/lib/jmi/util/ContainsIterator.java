// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.util;

import java.util.NoSuchElementException;
import javax.jmi.model.ModelElement;
import java.util.Collection;
import java.util.ArrayList;
import javax.jmi.model.GeneralizableElement;
import java.util.Iterator;

public class ContainsIterator implements Iterator {
	protected Iterator iterator;

	public ContainsIterator(final GeneralizableElement element) {
		super();
		ArrayList elements = new ArrayList();
		Collection supers = new ArrayList(element.allSupertypes());
		ModelElement temp;
		supers.add(element);
		for (Iterator it = supers.iterator(); it.hasNext();) {
			for (Iterator it2 = ((GeneralizableElement) it.next()).getContents().iterator(); it2.hasNext();) {
				temp = (ModelElement) it2.next();
				if (!elements.contains(temp)) {
					elements.add(temp);
				}
			}
		}
		iterator = elements.iterator();
	}

	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	public Object next() throws NoSuchElementException {
		return this.iterator.next();
	}

	public void remove() throws UnsupportedOperationException, IllegalStateException {
		throw new UnsupportedOperationException();
	}
}
