// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.util;

public class DebugException extends RuntimeException
{
    public DebugException() {
    }
    
    public DebugException(final String msg) {
        super(msg);
    }
}
