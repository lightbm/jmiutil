// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.NoSuchElementException;
import java.util.Iterator;

public class UnionQuery implements Query
{
    Query[] queries;
    
    public UnionQuery(final Query queryA, final Query queryB) {
        this.queries = new Query[] { queryA, queryB };
    }
    
    public UnionQuery(final Query[] queries) {
        this.queries = queries;
    }
    
    public Iterator iterator() {
        return new UnionIterator();
    }
    
    public boolean contains(final Object object) {
        for (int i = 0; i < this.queries.length; ++i) {
            if (this.queries[i].contains(object)) {
                return true;
            }
        }
        return false;
    }
    
    private class UnionIterator extends QueryIterator
    {
        int index;
        Iterator current;
        
        UnionIterator() {
            this.index = 0;
            this.current = null;
            if (UnionQuery.this.queries.length > 0) {
                this.current = UnionQuery.this.queries[0].iterator();
            }
        }
        
        public boolean hasNext() {
            if (this.current == null) {
                return false;
            }
            if (this.current.hasNext()) {
                return true;
            }
            ++this.index;
            if (this.index >= UnionQuery.this.queries.length) {
                this.current = null;
                return false;
            }
            this.current = UnionQuery.this.queries[this.index].iterator();
            return this.hasNext();
        }
        
        public Object next() {
            if (this.current == null) {
                throw new NoSuchElementException();
            }
            if (this.current.hasNext()) {
                return this.current.next();
            }
            ++this.index;
            if (this.index >= UnionQuery.this.queries.length) {
                this.current = null;
                throw new NoSuchElementException();
            }
            this.current = UnionQuery.this.queries[this.index].iterator();
            return this.next();
        }
    }
}
