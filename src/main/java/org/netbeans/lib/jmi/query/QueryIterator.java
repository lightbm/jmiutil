// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.Iterator;

abstract class QueryIterator implements Iterator
{
    public abstract boolean hasNext();
    
    public abstract Object next();
    
    public void remove() {
        throw new UnsupportedOperationException("MDR queries cannot be modified");
    }
    
    static class Delegate extends QueryIterator
    {
        protected final Iterator ITERATOR;
        
        public Delegate(final Iterator iterator) {
            this.ITERATOR = iterator;
        }
        
        public boolean hasNext() {
            return this.ITERATOR.hasNext();
        }
        
        public Object next() {
            return this.ITERATOR.next();
        }
    }
}
