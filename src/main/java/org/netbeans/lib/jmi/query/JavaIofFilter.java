// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

public class JavaIofFilter extends FilterQuery
{
    protected String featureName;
    private Class clazz;
    
    public JavaIofFilter(final Query query, final Class clazz) {
        super(query);
        this.clazz = null;
        this.clazz = clazz;
    }
    
    protected boolean accept(final Object object) {
        return this.clazz.isInstance(object);
    }
}
