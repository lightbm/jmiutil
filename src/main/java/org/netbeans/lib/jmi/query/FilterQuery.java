// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.NoSuchElementException;
import java.util.Iterator;

public abstract class FilterQuery implements Query
{
    private final Query QUERY;
    
    public FilterQuery(final Query query) {
        this.QUERY = query;
    }
    
    public Iterator iterator() {
        return new FilterIterator();
    }
    
    public boolean contains(final Object object) {
        return this.accept(object) && this.QUERY.contains(object);
    }
    
    protected abstract boolean accept(final Object p0);
    
    private class FilterIterator extends QueryIterator.Delegate
    {
        private Object next;
        
        FilterIterator() {
            super(FilterQuery.this.QUERY.iterator());
            this.next = null;
        }
        
        public boolean hasNext() {
            return this.ITERATOR.hasNext() && this.findNext();
        }
        
        public Object next() {
            if (this.next != null) {
                return this.next;
            }
            if (this.findNext()) {
                return this.next;
            }
            throw new NoSuchElementException();
        }
        
        private boolean findNext() {
            while (this.ITERATOR.hasNext()) {
                final Object object = this.ITERATOR.next();
                if (FilterQuery.this.accept(object)) {
                    this.next = object;
                    return true;
                }
            }
            this.next = null;
            return false;
        }
    }
}
