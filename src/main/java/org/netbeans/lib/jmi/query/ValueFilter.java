// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.Comparator;

public class ValueFilter extends FilterQuery
{
    private final Object VALUE;
    private final Comparator COMPARATOR;
    
    public ValueFilter(final Query query, final Object value) {
        this(query, value, null);
    }
    
    public ValueFilter(final Query query, final Object value, final Comparator comparator) {
        super(query);
        this.VALUE = value;
        this.COMPARATOR = comparator;
    }
    
    protected boolean accept(final Object object) {
        if (this.COMPARATOR != null) {
            return this.COMPARATOR.compare(this.VALUE, object) == 0;
        }
        if (this.VALUE == null) {
            return object == null;
        }
        return this.VALUE.equals(object);
    }
}
