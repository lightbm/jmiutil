// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.Iterator;
import java.util.Collection;

public class CollectionQuery implements Query
{
    private final Collection COLLECTION;
    
    public CollectionQuery(final Collection collection) {
        this.COLLECTION = collection;
    }
    
    public Iterator iterator() {
        return new QueryIterator.Delegate(this.COLLECTION.iterator());
    }
    
    public boolean contains(final Object object) {
        return this.COLLECTION.contains(object);
    }
}
