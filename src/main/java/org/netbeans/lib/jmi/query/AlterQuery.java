// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.Iterator;

public abstract class AlterQuery implements Query
{
    private final Query QUERY;
    private final Alternator ALTERNATOR;
    
    AlterQuery(final Query query) {
        this.QUERY = query;
        this.ALTERNATOR = null;
    }
    
    AlterQuery(final Query query, final Alternator alternator) {
        this.QUERY = query;
        this.ALTERNATOR = alternator;
    }
    
    public Iterator iterator() {
        return new AlterIterator();
    }
    
    public boolean contains(final Object object) {
        return this.QUERY.contains(this.alterBack(object));
    }
    
    protected Object alter(final Object object) {
        if (this.ALTERNATOR == null) {
            return object;
        }
        return this.ALTERNATOR.alter(object);
    }
    
    protected Object alterBack(final Object object) {
        if (this.ALTERNATOR == null) {
            return object;
        }
        return this.ALTERNATOR.alterBack(object);
    }
    
    private class AlterIterator extends QueryIterator.Delegate
    {
        private Object next;
        
        AlterIterator() {
            super(AlterQuery.this.QUERY.iterator());
            this.next = null;
        }
        
        public boolean hasNext() {
            return this.ITERATOR.hasNext();
        }
        
        public Object next() {
            final Object next = this.ITERATOR.next();
            return AlterQuery.this.alter(next);
        }
    }
    
    public interface Alternator
    {
        Object alter(final Object p0);
        
        Object alterBack(final Object p0);
    }
}
