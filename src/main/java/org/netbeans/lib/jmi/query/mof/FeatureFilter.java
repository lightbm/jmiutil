// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query.mof;

import javax.jmi.model.ModelElement;
import javax.jmi.model.NameNotFoundException;
import javax.jmi.reflect.RefObject;
import javax.jmi.model.StructuralFeature;
import javax.jmi.reflect.RefFeatured;
import java.util.Comparator;
import org.netbeans.lib.jmi.query.Query;
import javax.jmi.model.MofClass;
import javax.jmi.model.Feature;
import org.netbeans.lib.jmi.query.ValueFilter;

public class FeatureFilter extends ValueFilter
{
    protected String featureName;
    private Feature feature;
    private MofClass metaClazz;
    
    public FeatureFilter(final Query query, final String featureName, final Object value) {
        super(query, value);
        this.feature = null;
        this.metaClazz = null;
        this.featureName = featureName;
    }
    
    public FeatureFilter(final Query query, final String featureName, final Object value, final Comparator comparator) {
        super(query, value, comparator);
        this.feature = null;
        this.metaClazz = null;
        this.featureName = featureName;
    }
    
    protected boolean accept(final Object object) {
        if (!(object instanceof RefFeatured)) {
            return false;
        }
        this.resolveFeature((RefFeatured)object);
        if (this.feature == null) {
            return false;
        }
        final Object featureValue = ((RefFeatured)object).refGetValue((RefObject)this.feature);
        return super.accept(featureValue);
    }
    
    private void resolveFeature(final RefFeatured refFeatured) {
        final MofClass clazz = (MofClass)refFeatured.refMetaObject();
        if (this.metaClazz == null || !this.metaClazz.equals(clazz) || this.feature != null) {
            this.metaClazz = null;
            this.feature = null;
            try {
                this.metaClazz = clazz;
                final ModelElement me = clazz.lookupElementExtended(this.featureName);
                if (me instanceof Feature) {
                    this.feature = (Feature)me;
                }
            }
            catch (NameNotFoundException ex) {}
        }
    }
}
