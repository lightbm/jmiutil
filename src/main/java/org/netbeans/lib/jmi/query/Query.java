// 
// Decompiled by Procyon v0.5.36
// 

package org.netbeans.lib.jmi.query;

import java.util.Iterator;

public interface Query
{
    Iterator iterator();
    
    boolean contains(final Object p0);
}
